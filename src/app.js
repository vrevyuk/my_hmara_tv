/**
 * Created by Vitaly Revyuk on 6/4/19.
 */

import React from 'react';
import { HashRouter as Router, Route } from "react-router-dom";
import { createHashHistory } from 'history';
import { withConsumer } from './context';
import ReactGA from 'react-ga';

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';

import { createStyles } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';

import * as Views from './views';

const history = createHashHistory();

history.listen(location => {
	ReactGA.set({page: location.pathname});
	ReactGA.pageview(location.pathname);
});

const style = createStyles({
	container: {
		padding: 0,
	},
	root: {
		backgroundColor: '#fafafa',
	},

	drawer: {
		flexShrink: 0
	},

	desktopMenuContainer : {
		display: 'flex',
	},
});

class MyCloudTVApp extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			uk, en, ru, moment
		}
	}

	componentDidMount() {
		let { consumer: { restoreAuth } } = this.props;
		restoreAuth();
	}

	getRoutes() {
		let { consumer: { accessToken } } = this.props;

		return <React.Fragment>
			<Route exact path="/playlist" component={accessToken ? Views.PlayList : Views.LoginNoDialog} />
			<Route exact path="/feedback" component={Views.Feedback} />
			<Route exact path="/promo" component={Views.PromoCoupon} />
			<Route exact path="/referral" component={Views.Referral} />
			<Route exact path="/cameras" component={accessToken ? Views.Cameras : Views.LoginNoDialog} />
			<Route exact path="/contacts" component={accessToken ? Views.Contacts : Views.LoginNoDialog} />
			<Route exact path="/make-payment" component={accessToken ? Views.MakePayment : Views.LoginNoDialog} />
			<Route exact path="/packets" component={accessToken ? Views.UserPackets : Views.LoginNoDialog} />
			<Route exact path="/packets/:id" component={accessToken ? Views.PacketDetail : Views.LoginNoDialog} />
			<Route exact path="/transactions" component={accessToken ? Views.UserTransactions : Views.LoginNoDialog} />
			<Route exact path="/messages" component={accessToken ? Views.Messages : Views.LoginNoDialog} />
			<Route exact path="/login" component={accessToken ? Views.Main2 : Views.LoginNoDialog} />
			<Route exact path="/" component={Views.Main2} />
		</React.Fragment>
	}


	render() {
		let { consumer: { toggleDrawer, tryingRestoreAuth, drawerOpened, device } } = this.props;

		return tryingRestoreAuth
			? <Views.TryingRefreshAuthAlert />
			: <Container maxWidth="lg" style={style.container}><Paper style={style.root}>
				<Router history={history}>
					<Views.ApplicationBarWithSlider />

					<br />
					{device.isMobile ? (
						<React.Fragment>
							{this.getRoutes()}
							<Drawer
								style={style.drawer}
								variant="temporary"
								anchor="left"
								open={drawerOpened}
								onClose={toggleDrawer} >
								<Views.MenuList />
								<Divider />
							</Drawer>
						</React.Fragment>
					) : (
						<Grid container spacing={0}>
							<Grid item md={3} lg={3} xl={3} style={style.desktopMenuContainer}>
								<Views.MenuList />
							</Grid>
							<Grid item md={9} lg={9} xl={9}>
								{this.getRoutes()}
							</Grid>
						</Grid>
					)}

					<br />

					<Views.Footer />
				</Router>
			</Paper></Container>
	}
}

export default withConsumer(MyCloudTVApp);
