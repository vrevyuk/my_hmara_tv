/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import React from 'react';
import { withConsumer } from '../context';

import i18 from '../dictionary';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const cards = [
	{
		id: 'make-payment',
		title: i18.menuMakePayment, description: i18.menuMakePaymentDescription,
		image: 'https://www.vidyalayaschoolsoftware.com/content/vidimages/homepageimg/integration/f-online-payment.png'
	},
	{
		id: 'packets',
		title: i18.menuPackets, description: i18.menuPacketsDescription,
		image: 'https://cdn.wedevs.com/uploads/2014/03/Subscriptions.jpg'
	},
	{
		id: 'player',
		title: i18.menuPlayer, description: i18.menuPlayerDescription,
		image: 'http://hmara.tv/wp-content/uploads/2019/06/8.jpg'
	},
	{
		id: 'messages',
		title: i18.menuMessages, description: i18.menuMessagesDescription,
		image: 'https://jetpackme.files.wordpress.com/2016/02/jetpack-subscriptions.png?w=788'
	},
	{
		id: 'cameras',
		title: i18.menuCameras, description: i18.menuCamerasDescription,
		image: 'http://www.telecommunications-xxi.com.ua/wp-content/uploads/2019/05/montag-videonabludeniya.jpg'
	},
	{
		id: 'contacts',
		title: i18.menuContacts, description: i18.menuContactsDescription,
		image: 'https://phandroid.com/wp-content/uploads/2015/12/Google-Phone-Contacts-640x351.jpg'
	},
	{
		id: 'promo',
		title: i18.menuPromo, description: i18.menuPromoDescription,
		image: 'https://previews.123rf.com/images/djvstock/djvstock1511/djvstock151100087/47375563-shopping-promo-label-tag-graphic-design-vector-illustration-.jpg'
	},
];

const mainCardTemplate = {
	id: 'transactions',
	title: i18.menuHome,
	image: 'https://mk0zezosobuapu92jg73.kinstacdn.com/wp-content/uploads/2014/05/Financial-Balance.png'
};

function MediaCard(props = {}) {
	let { id, title, description, image, language, onClick = ()=>{} } = props;

	return (
		<Card id={id} style={{ minHeight: '40vh' }}>
			<CardActionArea onClick={()=>onClick(id)}>
				<CardMedia
					style={{ height: 150 }}
					image={image}
				/>
				<CardContent>
					<Typography gutterBottom variant="h5" component="h2">
						{title[language]}
					</Typography>
					<Typography variant="body2" color="textSecondary" component="p">
						{description[language]}
					</Typography>
				</CardContent>
			</CardActionArea>
		</Card>
	);
}

class Main extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}


	onClick(id) {
		let { consumer: { logOut } = {}, history } = this.props;

		switch (id) {
			case 'player': return window.open(`http://pc.hmara.tv/`);
			case 'home': return history.push('/');
			case 'login': return history.push('/login');
			case 'contacts': return history.push('/contacts');
			case 'make-payment': return history.push('/make-payment');
			case 'packets': return history.push('/packets');
			case 'cameras': return history.push('/cameras');
			case 'messages': return history.push('/messages');
			case 'transactions': return history.push('/transactions');
			case 'promo': return history.push('/promo');
			case 'logout': return logOut();
			default: break;
		}
	}

	render() {

		let { consumer: { language, balance, currency, device } } = this.props;
		let currencyName = i18[currency] ? i18[currency][language] : '';

		let mainCard = {
			...mainCardTemplate,
			title: i18.balance,
			description: Object.keys(i18.balance)
				.reduce( (result, key) => ({ ...result, [key]: `${balance || ''} ${currencyName}` }), {} )
		};

		let xs = device.isMobile ? 12 : 12;
		let sm = device.isMobile ? 12 : 6;
		let md = device.isMobile ? 12 : 3;
		let lg = device.isMobile ? 12 : 3;
		let xl = device.isMobile ? 12 : 3;

		return <Grid container justify="center" spacing={2}>
			{balance ? (
				<Grid xs={xs} sm={sm} md={md}  lg={lg} xl={xl} item>
					<MediaCard {...mainCard} language={language} onClick={this.onClick.bind(this)} />
				</Grid>
			) : null}
			{cards.map( (card, key) => <Grid key={key} xs={xs} sm={sm} md={md}  lg={lg} xl={xl} item>
				<MediaCard {...card} language={language} onClick={this.onClick.bind(this)} />
			</Grid>)}
		</Grid>

	}
}

export default withConsumer(Main);