/**
 * Created by Vitaly Revyuk on 6/5/19.
 */
import React from 'react';
import { withRouter } from 'react-router';
import { withConsumer } from '../context';
import i18 from '../dictionary';

import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';

import RemoveIcon from '@material-ui/icons/Delete';


class PlayListDevicesDialog extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		};
	}

	componentDidMount() {
		this.refreshList();
	}

	clickResetAllPlayLists() {
		let { consumer: { resetAllPlayLists, language } } = this.props;
		if (window.confirm(i18.activeDevicesConfirmDisallow[language])) {
			resetAllPlayLists();
		}
	}

	refreshList() {
		let { consumer: { getActiveDevices } } = this.props;
		getActiveDevices();
	}

	removeDevice(sessionId) {
		let { consumer: { deleteActiveDevice } } = this.props;
		deleteActiveDevice(sessionId);
	}

	render() {
		let { consumer: { loading, language, activeDevices }, toggleDialog } = this.props;

		return <Dialog
			open={true}
			onClose={toggleDialog}
			aria-labelledby="max-width-dialog-title">
			<DialogTitle id="max-width-dialog-title">{i18.activeDevicesBtn[language]}</DialogTitle>
			<Divider/>
			<DialogContent>
				<DialogContentText>
					&nbsp;&nbsp;&nbsp;{i18.activeDevicesDialogText1[language]}
				</DialogContentText>
				{' '}
				<DialogContentText>
					&nbsp;&nbsp;&nbsp;{i18.activeDevicesDialogText2[language]}
				</DialogContentText>
				{' '}
				<Typography>{loading ? i18.loading[language] : i18.activeDevicesListHeader[language]}</Typography>
				<List style={{width: '100%'}}>
					{activeDevices.map((session, key) => <ListItem key={key}>
						<ListItemText primary={`${session.deviceName.title} (${session.ip})`}
									  secondary={<span><b>{session.content}</b> <i>{session.subContent}</i></span>}/>
						<ListItemSecondaryAction>
							<IconButton edge="end" aria-label="Comments"
										disabled={loading}
										onClick={this.removeDevice.bind(this, session.id)}>
								<RemoveIcon/>
							</IconButton>
						</ListItemSecondaryAction>
					</ListItem>)}
				</List>
				{' '}
				<div style={{ display: 'flex', justifyContent: 'center' }}>
					<Button variant="outlined"
							onClick={this.clickResetAllPlayLists.bind(this)}
							color="secondary"
							disabled={loading}>
						{i18.cancelAllPlayLists[language]}
					</Button>
				</div>
			</DialogContent>
			<Divider />
			<DialogActions>
				<Button onClick={this.refreshList.bind(this)} color="primary" disabled={loading}>
					{i18.activeDevicesDialogBtnRefresh[language]}
				</Button>
				{' '}
				<Button onClick={toggleDialog} color="primary" disabled={loading}>
					{i18.activeDevicesDialogBtnClose[language]}
				</Button>
			</DialogActions>
		</Dialog>;
	}
}

export default withConsumer(withRouter(PlayListDevicesDialog));
