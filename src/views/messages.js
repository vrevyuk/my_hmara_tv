/**
 * Created by Vitaly Revyuk on 6/6/19.
 */
import React from 'react';
import { withConsumer } from '../context';
import { createStyles } from '@material-ui/core/styles';
// import i18 from '../dictionary';
import Loaging from './loading';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';

const style = createStyles({
	root: {
		width: '100%',
		overflowX: 'auto',
	},

	isReadHeader: {
		fontWeight: 'normal',
	},

	isNotReadHeader: {
		fontWeight: 'bold',
	},

	isReadText: {
		fontWeight: 'normal',
	},

	isNotReadText: {
		fontWeight: 'bold',
		color: 'green',
	},

});

class Messages extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			uk, en, ru
		}
	}

	componentDidMount() {
		let { consumer: { getUserMessages } } = this.props;
		getUserMessages();
	}

	onClick(message) {
		if (message.isRead === 1) return;
		let { consumer: { setUserMessageAsRead } } = this.props;
		setUserMessageAsRead(message.id);
	}

	render() {

		let { consumer: { userMessages = [], language } } = this.props;
		moment.locale(language || 'uk');

		return <div>
			<Loaging />
			<List>{userMessages.map( (row, key) => <ListItem key={key} onClick={this.onClick.bind(this, row)}>
				<ListItemText
					primary={<span style={row.isRead === 1 ? style.isReadHeader : style.isNotReadHeader}>
					{moment.unix(row.created).format('DD/MM/YY HH:mm')}
				</span>}
					secondary={<span style={row.isRead === 1 ? style.isReadText : style.isNotReadText}>
					<span dangerouslySetInnerHTML={{ __html: row.message }} />
				</span>} />
				<hr />
			</ListItem> )}</List>
		</div>
	}
}

export default withConsumer(Messages);