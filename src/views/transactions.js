/**
 * Created by Vitaly Revyuk on 6/6/19.
 */
import React from 'react';
import { withConsumer } from '../context';
import i18 from '../dictionary';
import Loaging from './loading';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';

import CallMade from '@material-ui/icons/CallMade';
import CallReceived from '@material-ui/icons/CallReceived';

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';

class UserTransactions extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			uk, en, ru
		}
	}

	componentDidMount() {
		let { consumer: { getUserTransactions } } = this.props;
		getUserTransactions();
	}

	render() {

		let { consumer: { transactions = [], language } } = this.props;

		moment.locale(language || 'uk');

		return <div>
			<Loaging />
			<List>{transactions.map( (row, key) => <ListItem key={key}>
				<ListItemAvatar>{row.sign === 1 ? <CallReceived color="primary"/> : <CallMade color="secondary"/>}</ListItemAvatar>
				<ListItemText
					primary={<span>
							{moment.unix(row.unixtime).format('DD/MM/YY HH:mm')}
						{' '}
						<b style={{ float: 'right'}}>{row.sum.toFixed(2)} {i18[row.currency][language]}</b>
						</span>}
					secondary={row.description} />
			</ListItem> )}</List>
		</div>;
	}
}

export default withConsumer(UserTransactions);