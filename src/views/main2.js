/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import React from 'react';
import { withRouter } from 'react-router';
import { withConsumer } from '../context';
import i18 from '../dictionary';

import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import MainTile from "./main-tile-template";
import PlayList from "@material-ui/icons/PlaylistPlay";
import { TileBalanceSVG, TileCameraSVG, TileContactsSVG, TileMessagesSVG, TilePaymentSVG, TilePortalSVG,
	TileSubscriptionsSVG } from "../assets";

const style = {
	rootGrid: {
		flexGrow: 1,
		padding: 10,
	},

	subtitleContainer: {
		width: '100%',
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		textAlign: 'center',
	},

	tileTitle: {
		color: 'white',
		textAlign: 'center',
	},

	tileSubTitle: {
		color: '#EEEEEE',
		textAlign: 'center',
	},
};

const tileBalance = (props = {}) => {

	let { consumer: { language = 'uk', balance = 0, currency = 'UAH' }, history } = props;

	return {
		Icon: <TileBalanceSVG height={50} width={50} />,
		Title: <Typography variant="h6" style={style.tileTitle}>{i18.balance[language]}</Typography>,
		SubTitle: <div style={style.subtitleContainer}>
			<Typography variant="subtitle1" style={style.tileSubTitle}>{balance} {i18[currency][language]}</Typography>
			<Typography variant="subtitle1" style={style.tileSubTitle}>{i18.menuTransactionsDescription[language]}</Typography>
		</div>,
		bgGradient: 'linear-gradient(55.38deg, #5470FB 10.82%, #C478FC 91.02%)',
		shadow: '0px 10px 30px rgba(162, 117, 251, 0.7)',
		onClick: () => history.push('/transactions'),
	};
};

const tilePayment = (props = {}) => {

	let { consumer: { language }, history } = props;

	return {
		Icon: <TilePaymentSVG height={50} width={50} />,
		Title: <Typography variant="h6" style={style.tileTitle}>{i18.menuMakePayment[language]}</Typography>,
		SubTitle: <div style={style.subtitleContainer}>
			<Typography variant="subtitle1" style={style.tileSubTitle}>{i18.menuMakePaymentDescription[language]}</Typography>
		</div>,
		bgGradient: 'linear-gradient(236.71deg, #30CCFD 3.28%, #3B79EA 97.57%)',
		shadow: '0px 10px 30px rgba(162, 117, 251, 0.7)',
		onClick: () => history.push('/make-payment'),
	};
};

const tileSubscribes = (props = {}) => {

	let { consumer: { language }, history } = props;

	return {
		Icon: <TileSubscriptionsSVG height={50} width={50} />,
		Title: <Typography variant="h6" style={style.tileTitle}>{i18.menuPackets[language]}</Typography>,
		SubTitle: <div style={style.subtitleContainer}>
			<Typography variant="subtitle1" style={style.tileSubTitle}>{i18.menuPacketsDescription[language]}</Typography>
		</div>,
		bgGradient: 'linear-gradient(231.5deg, #FAA991 0%, #B168DA 105.46%)',
		shadow: '0px 10px 30px rgba(162, 117, 251, 0.7)',
		onClick: () => history.push('/packets'),
	};
};

const tileMessages = (props = {}) => {

	let { consumer: { language }, history } = props;

	return {
		Icon: <TileMessagesSVG height={50} width={50} />,
		Title: <Typography variant="h6" style={style.tileTitle}>{i18.menuMessages[language]}</Typography>,
		SubTitle: <div style={style.subtitleContainer}>
			<Typography variant="subtitle1" style={style.tileSubTitle}>{i18.menuMessagesDescription[language]}</Typography>
		</div>,
		bgGradient: 'linear-gradient(231.5deg, #FAA991 0%, #B168DA 105.46%)',
		shadow: '0px 10px 30px rgba(162, 117, 251, 0.7)',
		onClick: () => history.push('/messages'),
	};
};

const tileCamera = (props = {}) => {

	let { consumer: { language }, history } = props;

	return {
		Icon: <TileCameraSVG height={50} width={50} />,
		Title: <Typography variant="h6" style={style.tileTitle}>{i18.menuCameras[language]}</Typography>,
		SubTitle: <div style={style.subtitleContainer}>
			<Typography variant="subtitle1" style={style.tileSubTitle}>{i18.menuCamerasDescription[language]}</Typography>
		</div>,
		bgGradient: 'linear-gradient(232.31deg, #24EAA6 -4.22%, #0CB7DE 101.84%)',
		shadow: '0px 10px 30px rgba(51, 247, 180, 0.7)',
		onClick: () => history.push('/cameras'),
	};
};

const tileContacts = (props = {}) => {

	let { consumer: { language }, history } = props;

	return {
		Icon: <TileContactsSVG height={50} width={50} />,
		Title: <Typography variant="h6" style={style.tileTitle}>{i18.menuContacts[language]}</Typography>,
		SubTitle: <div style={style.subtitleContainer}>
			<Typography variant="subtitle1" style={style.tileSubTitle}>{i18.menuContactsDescription[language]}</Typography>
		</div>,
		bgGradient: 'linear-gradient(231.89deg, #FFC31B 0%, #EE5F2C 100%)',
		shadow: '0px 10px 30px rgba(51, 180, 247, 0.7)',
		onClick: () => history.push('/contacts'),
	};
};

const tilePortal = (props = {}) => {

	let { consumer: { language, refreshToken, portal } } = props;

	return {
		Icon: <TilePortalSVG height={50} width={50} />,
		Title: <Typography variant="h6" style={style.tileTitle}>{i18.menuPlayer[language]}</Typography>,
		SubTitle: <div style={style.subtitleContainer}>
			<Typography variant="subtitle1" style={style.tileSubTitle}>{i18.menuPlayerDescription[language]}</Typography>
		</div>,
		bgGradient: 'linear-gradient(229.85deg, #9BC0D7 1.77%, #5789B1 101.49%)',
		shadow: '0px 10px 30px rgba(51, 180, 247, 0.7)',
		onClick: () => {
			const server_name = /hmara/g.test(portal) ? 'pc.hmara.tv'
				: /vivat/g.test(portal) ? 'pc.vivat.live'
					: 'google.com';
			return window.open(`http://${server_name}/?refreshToken=${refreshToken}`);
			// window.open(refreshToken ? `http://pc.hmara.tv/?refreshToken=${refreshToken}` : `http://pc.hmara.tv/`)
		},
	};
};

// const tilePromo = (props = {}) => {
//
// 	let { consumer: { language }, history } = props;
//
// 	return {
// 		Icon: <TilePromoSVG height={50} width={50} />,
// 		Title: <Typography variant="h6" style={style.tileTitle}>{i18.menuPromo[language]}</Typography>,
// 		SubTitle: <div style={style.subtitleContainer}>
// 			<Typography variant="subtitle1" style={style.tileSubTitle}>{i18.menuPromoDescription[language]}</Typography>
// 		</div>,
// 		bgGradient: 'linear-gradient(229.58deg, #FA588C 0%, #B239A1 103.31%)',
// 		shadow: '0px 10px 30px rgba(51, 180, 247, 0.7)',
// 		onClick: () => history.push('/promo'),
// 	};
// };

const tileReferral = (props = {}) => {

	let { consumer: { language }, history } = props;

	return {
		Icon: <TileBalanceSVG height={50} width={50} />,
		Title: <Typography variant="h6" style={style.tileTitle}>{i18.menuReferral[language]}</Typography>,
		SubTitle: <div style={style.subtitleContainer}>
			<Typography variant="subtitle2" style={style.tileSubTitle}>{i18.menuReferralDescription[language]}</Typography>
		</div>,
		bgGradient: 'linear-gradient(229.58deg, #FA588C 0%, #B239A1 103.31%)',
		shadow: '0px 10px 30px rgba(51, 180, 247, 0.7)',
		onClick: () => history.push('/referral'),
	};
};

const tilePlayLists = (props = {}) => {

	let {consumer: {language}, history} = props;

	return {
		Icon: <PlayList style={{height: 50, width: 50, color: 'white'}}/>,
		Title: <Typography variant="h6" style={style.tileTitle}>{i18.menuPlayList[language]}</Typography>,
		SubTitle: <div style={style.subtitleContainer}>
			<Typography variant="subtitle1" style={style.tileSubTitle}>{i18.menuPlayListDescription[language]}</Typography>
		</div>,
		bgGradient: 'linear-gradient(229.58deg, #FA588C 0%, #B239A1 103.31%)',
		shadow: '0px 10px 30px rgba(51, 180, 247, 0.7)',
		onClick: () => history.push('/playlist'),
	};
};

const tiles = [ tileBalance, tilePayment, tileSubscribes, tileMessages, tileCamera, tileContacts, tilePortal,
	tilePlayLists, tileReferral ];


const Main2 = function(props = {}) {

	return <div style={style.rootGrid}>
		<Grid container spacing={2}>
			{
				tiles.map( (tileFn, key) => <Grid key={key} item xs={12} sm={6} md={6} lg={4} xl={4}>
					<MainTile tile={{ ...tileFn(props) }} />
				</Grid>)
			}
		</Grid>
	</div>;

};

export default withConsumer(withRouter(Main2));
