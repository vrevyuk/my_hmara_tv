/**
 * Created by Vitaly Revyuk on 6/5/19.
 */
import React from 'react';
import { withConsumer } from '../context';

import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

class ToggleLanguage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			languages: ['en','uk','ru']
		}
	}

	handleClick(lang, e) {
		let { consumer: { setLanguage } } = this.props;

		switch (lang) {
			case 'show':
				this.setState({ anchorEl: e.currentTarget });
				break;
			case 'hide':
				this.setState({ anchorEl: null });
				break;
			case 'uk':
			case 'en':
			case 'ru':
				this.setState({ anchorEl: null }, () => setLanguage(lang) );
				break;
			default:
				break;
		}
	}

	render() {

		let { languages = [], anchorEl } = this.state;
		let { consumer: { language } } = this.props;

		return <div>
			<IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={this.handleClick.bind(this, 'show')}>
				{language === 'uk' && <img alt="uk" width={20} src={require('../assets/ukraine.png')} />}
				{language === 'en' && <img alt="en" width={20} src={require('../assets/united-states.png')} />}
				{language === 'ru' && <img alt="ru" width={20} src={require('../assets/russia.png')} />}
			</IconButton>
			<Menu
				id="simple-menu"
				anchorEl={anchorEl}
				keepMounted
				open={Boolean(anchorEl)}
				onClose={this.handleClick.bind(this, 'hide')} >
				{languages.map(
					(lang, key) =>
						<MenuItem key={key}
								  selected={language === lang}
								  onClick={this.handleClick.bind(this, lang)} >
							{lang.toUpperCase()}
						</MenuItem>
				)}

			</Menu>
		</div>;
	}
}

export default withConsumer(ToggleLanguage);