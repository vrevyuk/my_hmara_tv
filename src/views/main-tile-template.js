import React from 'react';
import { withRouter } from 'react-router';
import { withConsumer } from '../context';
import { createStyles } from '@material-ui/core';
import CardActionArea from '@material-ui/core/CardActionArea';
import svgData from '../assets/tile-wave';

function getStyles(tileProps = {}) {

    let { bgGradient, shadow } = tileProps;

    return createStyles({
        container: {
            minHeight: 250,
            background: bgGradient,
            boxShadow: shadow,
            borderRadius: 10,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'stretch',
        },

        iconContainer: {
            display: 'flex',
            borderTopRightRadius: 10,
            borderTopLeftRadius: 10,
            alignItems: 'center',
            justifyContent:'center',
            flexGrow: 1,
        },

        iconSubContainer: {
            backgroundColor: 'rgba(255,255,255,0.2)',
            padding: 20,
            borderRadius: '50%',
            display: 'flex',
            alignItems: 'center',
            justifyContent:'center',
        },

        titleContainer: {
            display: 'flex',
            flexDirection: 'column',
            borderBottomRightRadius: 10,
            borderBottomLeftRadius: 10,
            flexGrow: 2,
            padding: 5,
            justifyContent:'space-around',

            backgroundImage: `url("data:image/svg+xml,${svgData}")`,
            backgroundPosition: 'bottom',
            backgroundRepeat: 'repeat-x',
            backgroundSize: 'contain',
        },

        titleSubContainer: {
        },
    });
}

class BalanceTile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hover: false
        }
    }

    toggleHover(flag) {
        this.setState({ hover: flag });
    }

    render() {

        let { hover } = this.state;
        let { tile = {} }  = this.props;
        let { Icon, Title, SubTitle, onClick } = tile;

        let action = typeof onClick === "function" ? onClick : () => {};

        let style = getStyles({ ...tile, shadow: hover ? tile.shadow : '' });

        return (
            <CardActionArea style={style.container}
                            onClick={() => setTimeout(action, 250)}
                            onMouseOver={this.toggleHover.bind(this, true)}
                            onMouseOut={this.toggleHover.bind(this, false)}>
                <div style={style.iconContainer}>
                    <div style={style.iconSubContainer}>
                        {Icon} {hover}
                    </div>
                </div>
                <div style={style.titleContainer}>
                    {Title}
                    {SubTitle}
                </div>
            </CardActionArea>
        );
    }
}

export default withConsumer(withRouter(BalanceTile));