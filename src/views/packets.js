/**
 * Created by Vitaly Revyuk on 6/6/19.
 */
import React from 'react';
import { Link } from 'react-router-dom';
import { withConsumer } from '../context';
import i18 from '../dictionary';
import Loaging from './loading';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Divider from '@material-ui/core/Divider';

import Check from '@material-ui/icons/Check';
import AttachMoney from '@material-ui/icons/AttachMoney';
import Add from '@material-ui/icons/AddCircle';
import Delete from '@material-ui/icons/RemoveCircle';

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';

class UserPackets extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			uk, en, ru
		}
	}

	componentDidMount() {
		let { consumer: { getUserPackets } } = this.props;
		getUserPackets();
	}

	componentDidUpdate(prevProps, prevState) {
		let { consumer: { language: prevLanguage, getUserPackets } } = this.props;
		let { consumer: { language: nextLanguage } } = prevProps;
		if (prevLanguage !== nextLanguage) getUserPackets();
	}

	onClick(packetId, e) {
		let { consumer: { subscribePacket, unSubscribePacket, language } } = this.props;
		let { id } = e.currentTarget;

		switch (id) {
			case 'subscribe': if (window.confirm(i18.confirmSubscribe[language])) subscribePacket(packetId); break;
			case 'unsubscribe': if (window.confirm(i18.confirmUnSubscribe[language])) unSubscribePacket(packetId); break;
			default: break;
		}
	}

	renderPacket(packet, key, packets) {

		let { consumer: { loading, language, device } } = this.props;
		let { currency, expirationDate, expired, fixed, groupId, id, inCountry, period, price, subscribed, title } = packet;

		return <React.Fragment key={key}>
			{packets[key - 1] && groupId !== packets[key - 1].groupId ? <p>&nbsp;</p> : null}
			<ListItem key={key}>
				{device.isMobile ? (
					<IconButton disabled={Boolean(fixed) || !inCountry || loading}
							id={subscribed ? 'unsubscribe':'subscribe'}
							onClick={this.onClick.bind(this, id)}>
						{subscribed ? <Delete /> : <Add />}
					</IconButton>
				) : (
					<ListItemIcon>
						{
							subscribed && expired ? <IconButton disabled={loading}><AttachMoney color="error"/></IconButton>
								: !subscribed ? <Typography/> : <IconButton disabled={loading}><Check color="primary"/></IconButton>
						}
					</ListItemIcon>
				)}
				<ListItemText
					primary={<Typography>
						<Link to={`/packets/${id}`}>{title}</Link>
						{' '}
						<span style={{ color: 'purple' }}>{price.toFixed(2)} {i18[currency][language]} {i18[period][language]}</span>
					</Typography>}
					secondary={!inCountry ? i18.not_available_in_our_country[language]
						: subscribed && !expired ? `${i18.expired[language]} ${moment.unix(expirationDate).format('DD MMMM YYYY hh:mm')}`
						: subscribed && expired ? `required for payment`
						: <span>&nbsp;</span>}
				/>
				{!device.isMobile && (
					<ListItemSecondaryAction>
						<Button disabled={Boolean(fixed) || !inCountry || loading}
								id={subscribed ? 'unsubscribe':'subscribe'}
								onClick={this.onClick.bind(this, id)}>
							{subscribed ? i18.unsubscribeBtn[language] : i18.subscribeBtn[language]}
						</Button>
					</ListItemSecondaryAction>
				)}
			</ListItem>
			{key < packets.length - 1 ? <Divider variant="inset" component="li" /> : null}
		</React.Fragment>
	}

	render() {

		let { consumer: { packets = [], language } } = this.props;
		moment.locale(language || 'uk');

		packets = packets.find(p => p.groupId === 101) ? packets.filter( p => p.groupId !== 100) : packets;

		return <div>
			<Loaging />
			<List>{packets.map(this.renderPacket.bind(this))}</List>
		</div>

	}
}

export default withConsumer(UserPackets);