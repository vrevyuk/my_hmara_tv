/**
 * Created by Vitaly Revyuk on 6/5/19.
 */
import React from 'react';
import { withConsumer } from '../context';
import i18 from '../dictionary';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class AddContact extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			haveAuthCode: false,
			contact: '',
			code: '',
		}
	}

	componentDidMount() {
		let { consumer: { cancelAction } } = this.props;
		cancelAction();
	}

	getCode() {
		let { contact } = this.state;
		if (!contact) return;
		let { consumer: { addUserContact } } = this.props;
		this.setState({ code: '' }, () => {
			addUserContact({ contact });
		});
	}

	checkCode() {
		let { contact, code } = this.state;
		if (!contact || !code) return;
		let { consumer: { addUserContact } } = this.props;
		addUserContact({ contact, code });
	}

	cancel() {
		let { consumer: { cancelAction, closeAddContactDialog, haveAuthCode } } = this.props;
		haveAuthCode ? cancelAction() : closeAddContactDialog();
	}

	onChange(e) {
		let { id, value } = e.target;
		this.setState({ [id]: value });
	}

	render() {
		let { consumer: { haveAuthCode, loading, language, addContactDialog } } = this.props;
		let { contact, code } = this.state;

		return (<Dialog open={addContactDialog}>
			<DialogTitle id="form-dialog-title">{i18.addNewContactHeader[language]}</DialogTitle>
			<DialogContent>
				<DialogContentText>{i18.addNewContactText[language]}</DialogContentText>

				<TextField
					autoFocus={!haveAuthCode}
					margin="dense"
					id="contact"
					label="Phone or email address"
					type="text"
					fullWidth
					disabled={haveAuthCode || loading}
					onChange={this.onChange.bind(this)}
					value={contact}
				/>

				{haveAuthCode ? <TextField
					autoFocus={haveAuthCode}
					margin="dense"
					id="code"
					label="code from sms or email"
					type="text"
					fullWidth
					disabled={loading}
					onChange={this.onChange.bind(this)}
					value={code}
				/> : null}

			</DialogContent>
			<DialogActions>
				<Button onClick={this.cancel.bind(this)} color="primary">{i18.cancelBtn[language]}</Button>

				{ !haveAuthCode ? <Button onClick={this.getCode.bind(this)} color="primary" disabled={loading}>{i18.getCodeBtn[language]}</Button> : null }

				{ haveAuthCode ? <Button onClick={this.checkCode.bind(this)} color="primary" disabled={loading}>{i18.checkCodeBtn[language]}</Button> : null }
			</DialogActions>
		</Dialog>);
	}
}

export default withConsumer(AddContact);