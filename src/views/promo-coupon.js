/**
 * Created by Vitaly Revyuk on 6/6/19.
 */
import React from 'react';
import { withConsumer } from '../context';
import i18 from '../dictionary';
import i18d from '../depend_on_portal_dictionary';
import Login from './login';
import LoginNoDialog from './login-no-dialog';

import { green } from '@material-ui/core/colors';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import CardGiftcard from '@material-ui/icons/CardGiftcard';
import DateRange from '@material-ui/icons/DateRange';
import Clear from '@material-ui/icons/Clear';
import LiveTv from '@material-ui/icons/LiveTv';

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';
import packets from "./packets";

class PromoCoupon extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			uk, en, ru,
		}
	}

	componentDidMount() {

		let {
			location: { search = '' } = {},
			consumer: { getCouponData, showLoginBox },
		} = this.props;

		showLoginBox(false);

		let { promo } = search
			.replace(/^\?/,'')
			.split('&')
			.map(param => ({ [param.split('=')[0]]: param.split('=')[1] }))
			.find(item => item.hasOwnProperty('promo')) || {};

		promo && this.setState({ couponFromUrl: promo }, () => getCouponData(promo) )
	}

	componentWillUnmount() {
		let { consumer: { coupon, setValues } } = this.props;
		if (coupon) setTimeout( () => setValues({ coupon: false, applyCouponSuccess: false, applyCouponError: null }), 500 );
	}

	checkCoupon() {
		let { consumer: { getCouponData } } = this.props;
		let { couponFromTextField } = this.state;
		couponFromTextField && getCouponData(couponFromTextField);
	}

	onChangeText(e) {
		let { consumer: { cancelAction } } = this.props;
		let { id, value } = e.target;
		this.setState({ [id]: value }, () => cancelAction() );
	}

	continueRegistration() {
		let { consumer: { accessToken, applyCoupon, showLoginBox } } = this.props;

		accessToken ? applyCoupon() : showLoginBox(true);
	}

	useLater() {
		let { consumer: { setValues } } = this.props;

		this.setState(
			{ couponFromTextField: '', couponFromUrl: '' },
			() => setValues({ coupon: false, applyCouponSuccess: false, applyCouponError: null })
		);

	}

	goToPortalInstall() {
		window.open('https://hmara.tv/#previousboxes', '_new');
	}

	showCouponData(coupon = {}) {
		let { packets, test, discount } = coupon;
		let { consumer: { language } } = this.props;

		return <List>
			{packets && packets.length > 0 ? (
				packets.map((packetTitle, key) => <ListItem key={`p_${key}`}>
					<ListItemAvatar>
						<Avatar>
							<LiveTv color="secondary" />
						</Avatar>
					</ListItemAvatar>
					<ListItemText primary={packetTitle} secondary={i18.promoPacket[language]} />
				</ListItem>)
			) : null}
			{test && <ListItem>
				<ListItemAvatar>
					<Avatar>
						<DateRange color="primary" />
					</Avatar>
				</ListItemAvatar>
				<ListItemText primary={i18.promoFreePeriod[language]} secondary={`${i18.promoFor[language]} ${test.value} ${i18[`promo${test.period}`][language]}`} />
			</ListItem>}
			{discount && <ListItem>
				<ListItemAvatar>
					<Avatar>
						<CardGiftcard color="primary" />
					</Avatar>
				</ListItemAvatar>
				<ListItemText primary={i18.promoDiscount[language]} secondary={`${discount.percent}${'%'} ${i18.promoFor[language]} ${discount.value} ${i18[`promo${discount.period}`][language]}`} />
			</ListItem>}
			{/*{!test && !discount && <ListItem>*/}
			{/*	<ListItemAvatar>*/}
			{/*		<Avatar>*/}
			{/*			<Clear color="secondary" />*/}
			{/*		</Avatar>*/}
			{/*	</ListItemAvatar>*/}
			{/*	<ListItemText primary={i18.promoNothing[language]} />*/}
			{/*</ListItem>}*/}
		</List>
	}

	render() {

		let { consumer: { accessToken, loading, language, coupon, applyCouponSuccess, applyCouponError, isLoginBoxShows } } = this.props;
		let { couponFromUrl, couponFromTextField } = this.state;
		moment.locale(language || 'de');

		return isLoginBoxShows && !applyCouponSuccess ? <LoginNoDialog /> : <div>
			<div style={{ padding: 10 }}>
				<p />
				<Typography>
					{i18d().promoDialogText[language]}
				</Typography>

				<TextField
					id="couponFromTextField"
					label={i18.promoDialogHeader[language]}
					value={couponFromTextField || couponFromUrl || ''}
					disabled={!!couponFromUrl || loading}
					onChange={this.onChangeText.bind(this)}
					margin="normal"
					variant="outlined"
					 />

				<p />
				{loading ? <Typography>{i18.loading[language]}</Typography>
					: coupon && <div>{this.showCouponData(coupon)}</div>}

				{applyCouponError && <React.Fragment>
					<p />
					<Typography color="secondary">{applyCouponError}</Typography>
					<p/>
					<Button id="disagree"
						// onClick={this.useLater.bind(this)}
							onClick={this.goToPortalInstall.bind(this)}
							color="primary"
							variant="contained"
							disabled={loading}>
						{i18.goToPortalInstallation[language]}
					</Button>
				</React.Fragment>}

				<p />
				{coupon && applyCouponSuccess ? (
					<div style={{ textAlign: 'left' }}>
						<Typography style={{ color: green[600] }}>{i18.promoSuccessful[language]}</Typography>
						<p />
						<Button id="disagree"
								// onClick={this.useLater.bind(this)}
								onClick={this.goToPortalInstall.bind(this)}
								color="primary"
								variant="contained"
								disabled={loading}>
							{i18.goToPortalInstallation[language]}
						</Button>
					</div>
				) : (
					<div style={{ textAlign: 'left' }}>
						{' '}
						{ couponFromUrl || coupon ? (
							<React.Fragment>
								<Button id="disagree"
										variant="contained"
										onClick={this.useLater.bind(this)}
										color="primary"
										disabled={loading}>
									{i18.cancelBtn[language]}
								</Button>
								{' '}
								{!applyCouponError && <Button id="agree"
															  variant="contained"
															  onClick={this.continueRegistration.bind(this)}
															  color="primary"
															  disabled={!coupon || loading}>
									{i18.promoContinueBtn[language]}
								</Button>}
							</React.Fragment>
						) : (
							<Button variant="contained"
									onClick={this.checkCoupon.bind(this)}
									disabled={loading || !couponFromTextField}>
								{i18.promoCheckBtn[language]}
							</Button>
						) }
					</div>
				)}
			</div>
			{/*{isLoginBoxShows && !applyCouponSuccess ? <Login /> : null}*/}
		</div>;
	}
}

export default withConsumer(PromoCoupon);