/**
 * Created by Vitaly Revyuk on 6/5/19.
 */
import React from 'react';
import { withRouter } from 'react-router';
import { withConsumer } from '../context';
import i18 from '../dictionary';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class Login extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			haveAuthCode: false,
			open: true,
			contact1: '',
			contact: '',
			code: '',
		}
	}

	componentDidMount() {
		let { consumer: { cancelAction, coupon } } = this.props;
		if (!coupon) cancelAction();
	}

	getCode() {
		let { contact } = this.state;
		if (!contact) return;
		this.setState({ loading: true }, () => {
			let { consumer: { getAuthCode } } = this.props;
			getAuthCode({contact});
		});
	}

	checkCode() {
		let { contact, code } = this.state;
		if (!contact || !code) return;
		this.setState({ loading: true }, () => {

			let { consumer: { referral, checkAuthCode } } = this.props;

			checkAuthCode({ contact, code, referral });
		});
	}

	cancel() {
		let { consumer: { cancelAction, showLoginBox, haveAuthCode, coupon }, history } = this.props;
		haveAuthCode ? cancelAction() :
			coupon ? showLoginBox(false) : history.push("/");
	}

	onChange(e) {
		let { id, value } = e.target;
		this.setState({ [id]: value });
	}

	render() {
		let { consumer: { haveAuthCode, loading, language } = {} } = this.props;

		let { open, contact, code } = this.state;

		return (<Dialog open={open}>
			<DialogTitle id="form-dialog-title">
				{i18.loginHeader[language]}
			</DialogTitle>

			<DialogContent>
				<DialogContentText>{i18.loginText[language]}</DialogContentText>
				<TextField
					autoFocus={!haveAuthCode}
					margin="dense"
					id="contact"
					label={i18.contactPlaceholder[language]}
					type="text"
					fullWidth
					disabled={haveAuthCode || loading}
					onChange={this.onChange.bind(this)}
					value={contact}
				/>

				{haveAuthCode ? <TextField
					autoFocus={haveAuthCode}
					margin="dense"
					id="code"
					label={i18.codePlaceholder[language]}
					type="text"
					fullWidth
					disabled={loading}
					onChange={this.onChange.bind(this)}
					value={code}
				/> : null}

			</DialogContent>
			<DialogActions>
				<Button onClick={this.cancel.bind(this)} color="primary">{i18.cancelBtn[language]}</Button>

				{ !haveAuthCode ? <Button onClick={this.getCode.bind(this)} color="primary" disabled={loading}>{i18.getCodeBtn[language]}</Button> : null }

				{ haveAuthCode ? <Button onClick={this.checkCode.bind(this)} color="primary" disabled={loading}>{i18.checkCodeBtn[language]}</Button> : null }
			</DialogActions>
		</Dialog>);
	}
}

export default withConsumer(withRouter(Login));