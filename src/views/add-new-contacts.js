/**
 * Created by Vitaly Revyuk on 6/6/19.
 */
import React from 'react';
import { withConsumer } from '../context';
import i18 from '../dictionary';

import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';

import AddIcon from '@material-ui/icons/Add'

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';

class AddNewContact extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			uk, en, ru,
		}
	}

	componentDidMount() {
		let { consumer: { addUserContact } } = this.props;
	}

	enableStepper() {
		this.setState({ enableStepper: true });
	}

	render() {

		let { consumer: { loading, language } } = this.props;
		let { enableStepper, steps, activeStep, } = this.state;
		moment.locale(language || 'uk');

		return <React.Fragment>
			<Fab id="enableStepper" onClick={this.enableStepper.bind(this)} color="primary"><AddIcon/></Fab>
		</React.Fragment>;
	}
}

export default withConsumer(AddNewContact);