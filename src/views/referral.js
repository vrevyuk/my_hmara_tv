/**
 * Created by Vitaly Revyuk on 6/6/19.
 */
import React from 'react';
import { Redirect } from 'react-router';
import { withConsumer } from '../context';
import i18 from '../dictionary';
import i18d from '../depend_on_portal_dictionary';

import Button from '@material-ui/core/Button';

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';
import {Typography} from "@material-ui/core";

class Referral extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			uk, en, ru
		}
	}

	handleClose(e) {

		let { id } = e.currentTarget;
		let {
			history,
			location: { search = '' } = {},
			consumer: { setReferral } = {}
		} = this.props;

		if(id === 'agree') {
			let { referral } = search
				.replace(/^\?/,'')
				.split('&')
				.map(param => ({ [param.split('=')[0]]: param.split('=')[1] }))
				.find(item => item.hasOwnProperty('referral')) || {};

			// console.log({ referral });
			setReferral(referral);

		}

		history.replace('/login');
	}

	render() {

		let {
			location: {search = '' } = {},
			consumer: {accessToken, language, referralUrl, portal}
		} = this.props;

		let {referral} = search
			.replace(/^\?/,'')
			.split('&')
			.map(param => ({ [param.split('=')[0]]: param.split('=')[1] }))
			.find(item => item.hasOwnProperty('referral')) || {};

		moment.locale(language || 'uk');

		return accessToken ? (
			<div style={{ padding: 10 }}>
				<Typography variant='h5'>
					{i18.menuReferral[language]}
				</Typography>

				<br/>
				<Typography>
					{i18d().menuReferralDescriptionFull[language]}
				</Typography>
				<br/>
				<br/>
				<div style={{color: 'blue'}}>{referralUrl}</div>
			</div>
		) : referral ? (
			<div style={{ padding: 10 }}>
				<Typography variant='h5'>
					{i18.menuReferral[language]}
				</Typography>

				<br/>
				<Typography>
					{i18d().referralDialogText[language]}
				</Typography>

				<br/>
				<Button id="agree" variant="contained" onClick={this.handleClose.bind(this)} color="primary">
					{i18.referralDialogAgreeBtn[language]}
				</Button>
			</div>
		) : ( <Redirect to="/login" />)
	}
}

export default withConsumer(Referral);