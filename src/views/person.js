/**
 * Created by Vitaly Revyuk on 6/5/19.
 */
import React from 'react';
import { withRouter } from 'react-router';
import { withConsumer } from '../context';
import i18 from '../dictionary';

import IconButton from "@material-ui/core/IconButton";

import Person from '@material-ui/icons/AccountBoxOutlined';
import Close from '@material-ui/icons/ExitToApp';


class PersonBtn extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	login() {
		let { history } = this.props;
		history.push('/login');
	}

	logout() {
		let { consumer: { logOut, language } } = this.props;
		window.confirm(i18.doYouWantToChangeUser[language]) && logOut();
	}

	render() {
		let { consumer: { accessToken } } = this.props;

		return accessToken
			? <IconButton onClick={this.logout.bind(this)}><Close /></IconButton>
			: <IconButton onClick={this.login.bind(this)}><Person /></IconButton>;
	}
}

export default withConsumer(withRouter(PersonBtn));