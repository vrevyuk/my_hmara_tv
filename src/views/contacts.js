/**
 * Created by Vitaly Revyuk on 6/6/19.
 */
import React from 'react';
import { withConsumer } from '../context';
import { AddContact } from './index';
import Loaging from './loading';

import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Typography from '@material-ui/core/Typography'
import Fab from '@material-ui/core/Fab';

import ContactMail from '@material-ui/icons/ContactMail'
import ContactPhone from '@material-ui/icons/ContactPhone'
import AddIcon from '@material-ui/icons/Add'

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';

class Contacts extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			uk, en, ru,
		}
	}

	componentDidMount() {
		let { consumer: { getUserContacts } } = this.props;
		getUserContacts();
	}

	openAddDialog() {
		let { consumer: { openAddContactDialog } } = this.props;
		openAddContactDialog();
	}

	render() {

		let { consumer: { contacts = [], language } } = this.props;
		moment.locale(language || 'uk');

		return <React.Fragment>
			<Loaging />
			<List>{
				contacts.map( (contact, key) => <ListItem key={key}>
					<ListItemIcon>
						{/@/i.test(contact) ? <ContactMail /> : <ContactPhone />}
					</ListItemIcon>
					<ListItemText>
						<Typography>{contact}</Typography>
					</ListItemText>
				</ListItem>)
			}</List>

			<Fab onClick={this.openAddDialog.bind(this)} color="primary"><AddIcon/></Fab>

			<AddContact />
		</React.Fragment>;
	}
}

export default withConsumer(Contacts);