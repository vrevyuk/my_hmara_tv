/**
 * Created by Vitaly Revyuk on 6/6/19.
 */
import React from 'react';
import { withConsumer } from '../context';
import { createStyles } from '@material-ui/core/styles';
import i18 from '../dictionary';
import i18d from '../depend_on_portal_dictionary';
import Loaging from './loading';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/AddCircle';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';
import {ListItemSecondaryAction} from "@material-ui/core";

const style = createStyles({
	root: {
		width: '100%',
		overflowX: 'auto',
	},

	avatar: {
		height: 150,
		width: 150,
		margin: 10,
	},
});

class Cameras extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			uk, en, ru
		}
	}

	componentDidMount() {
		let { consumer: { getUserCams } } = this.props;
		getUserCams();
	}

	deleteCam(camera) {
		if (!camera) return;
		let { consumer: { language, deleteUserCam } } = this.props;
		let { cameraId } = camera;
		if (window.confirm(i18.deleteCamera[language])) {
			console.log(`delete ${cameraId}`);
			deleteUserCam(camera);
		}
	}

	editTitle(camera) {
		if (!camera) return;
		let { consumer: { language, updateUserCam } } = this.props;
		let { cameraId, title } = camera;
		let newTitle = window.prompt(i18.editCameraTitle[language], title);
		if (newTitle) {
			console.log(cameraId, newTitle);
			updateUserCam(cameraId, { title: newTitle });
		}
	}

	editDescription(camera) {
		if (!camera) return;
		let { consumer: { language, updateUserCam } } = this.props;
		let { cameraId, description } = camera;
		let newDescription = window.prompt(i18.editCameraDescription[language], description);
		if (newDescription) {
			console.log(cameraId, newDescription);
			updateUserCam(cameraId, { description: newDescription });
		}
	}

	editSrc(camera) {
		if (!camera) return;
		let { consumer: { language, updateUserCam } } = this.props;
		let { cameraId, src } = camera;
		let newSrc = window.prompt(i18.editCameraSrc[language], src);
		if (newSrc) {
			console.log(cameraId, newSrc);
			updateUserCam(cameraId, { src: newSrc });
		}
	}

	visibilityCam(camera) {
		if (!camera) return;
		let { consumer: { updateUserCam } } = this.props;
		let { cameraId, disabled } = camera;
		updateUserCam(cameraId, { disabled: !disabled });
	}

	addCam() {
		let { consumer: { language, addUserCam } } = this.props;
		let title = window.prompt(i18.editCameraTitle[language]);
		if (!title) return;
		let description = window.prompt(i18.editCameraDescription[language]);
		if (!description) return;
		let src = window.prompt(i18.editCameraSrc[language]);
		if (!src) return;
		addUserCam({ title, description, src });
	}

	render() {

		let { consumer: { loading, cameras = [], language, setting: { imagesServer }, device: { isMobile } } } = this.props;
		moment.locale(language || 'uk');

		return <div style={style.root}>
            <Loaging />
			<List>{cameras.map( (cam, key) => <React.Fragment key={key}>
				<ListItem alignItems="flex-start">
					{!isMobile && <ListItemAvatar>
						<Tooltip title={i18.cameraTooltipScreenShot[language]}>
							<Avatar style={style.avatar} src={`${imagesServer}/${cam.image}`} />
						</Tooltip>
					</ListItemAvatar>}

					<ListItemText>
						<Typography noWrap variant="h6">
							<Tooltip title={i18.cameraTooltipDelete[language]}>
								<IconButton onClick={this.deleteCam.bind(this, cam)} disabled={loading}><Delete /></IconButton>
							</Tooltip>

							<Tooltip title={i18.cameraTooltipHide[language]}>
								<IconButton onClick={this.visibilityCam.bind(this, cam)} disabled={loading}>
									{cam.disabled ? <VisibilityOff /> : <Visibility />}
								</IconButton>
							</Tooltip>

							<Tooltip title={i18.cameraTooltipTitle[language]}>
								<IconButton onClick={this.editTitle.bind(this, cam)} disabled={loading}><Edit /></IconButton>
							</Tooltip>

							{cam.title}
						</Typography>
						<Typography noWrap variant="subtitle1">
							<Tooltip title={i18.cameraTooltipSrc[language]}>
								<IconButton onClick={this.editSrc.bind(this, cam)} disabled={loading}><Edit /></IconButton>
							</Tooltip>
							 {cam.src}
						</Typography>
						<Typography noWrap variant="body2">
							<Tooltip title={i18.cameraTooltipDescription[language]}>
								<IconButton onClick={this.editDescription.bind(this, cam)} disabled={loading}><Edit /></IconButton>
							</Tooltip>
							{cam.description}
						</Typography>
					</ListItemText>
				</ListItem>
				<Divider />
			</React.Fragment> )}
			<ListItem onClick={this.addCam.bind(this)} disabled={loading}>
				<ListItemText
					primary={<div><IconButton><Add /></IconButton> {i18.newCamera[language]}</div>}
				/>
				{' '}
				<ListItemSecondaryAction>

				</ListItemSecondaryAction>
			</ListItem>
			</List>
			<br />
			<Paper>
				<Typography dangerouslySetInnerHTML={{ __html: i18d().newCameraText[language] }} />
			</Paper>
			</div>

	}
}

export default withConsumer(Cameras);