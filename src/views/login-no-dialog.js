/**
 * Created by Vitaly Revyuk on 6/5/19.
 */
import React, {useEffect, useReducer, useState} from 'react';
import { withRouter } from 'react-router';
import { withConsumer } from '../context';
import i18 from '../dictionary';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import {InputBase} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import makeStyles from "@material-ui/core/styles/makeStyles";

import EditIcon from "@material-ui/icons/Edit";
import LockIcon from "@material-ui/icons/Lock";
import UnLockIcon from "@material-ui/icons/LockOpen";
import KeybIcon from "@material-ui/icons/Keyboard";
import PersonIcon from "@material-ui/icons/AccountBoxOutlined";

const useStyles = makeStyles((theme) => ({
	root: {
		padding: 40,
		margin: 50,
		// backgroundImage: 'url("https://pluspng.com/img-png/wave-background-png-blue-moving-flowing-abstract-waves-on-white-background-blurred-smooth-seamless-loop-design-video-animation-1920x1080-motion-background-videoblocks-1920.png")',
		// backgroundPosition: 'center center',
		// backgroundSize: 'cover',
		// backgroundRepeat: 'no-repeat'
	},
	header: {
		textAlign: 'center',
	},
	inputRoot: {
		display: 'flex',
		alignItems: 'center',
		width: '100%',
	},
	inputContact: {
		width: 400
	},
	inputCode: {
		width: 100
	},
	iconButton: {
		padding: 10,
	},
	buttonContainer: {
	},
}));

const reducer = (state, action) => {
	switch (action.type) {
		case 'contact': return  {...state, contact: action.payload};
		case 'code': return  {...state, code: action.payload};
		case 'decreaseCountDown': return  {...state, countDown: state.countDown - 1};
		case 'resetCountDown': return  {...state, countDown: 60};
		case 'stopCountDown': return  {...state, countDown: 0};
		default: return state;
	}
};

const initialState = {
	contact: '',
	code: '',
	countDown: 0
}

function Login2(props) {
	const classes = useStyles();
	const [state, dispatch] = useReducer(reducer, initialState, (state) => state);
	const {contact, code, countDown} = state;
	let {consumer: {
		haveAuthCode, codeData, loading, language, getAuthCode, referral, checkAuthCode, cancelAction, showLoginBox, coupon
	} = {}, history} = props;


	useEffect(() => {
		if (!countDown) return;
		const timerId = setTimeout(() => {
			dispatch({type: 'decreaseCountDown'});
		}, 1000);
		return () => clearTimeout(timerId);
	}, [countDown]);

	const getCode = () => {
		if (contact && contact.length >= 6 && countDown === 0) {
			dispatch({type: 'resetCountDown'});
			getAuthCode({contact});
		}
	};

	const checkCode = () => {
		if (code) {
			checkAuthCode({contact, code, referral});
		}
	};

	const cancelContact = () => {
		haveAuthCode ? cancelAction()
			: coupon ? showLoginBox(false)
			: history.push("/");
	};

	return (
		<Paper elevation={3} className={classes.root}>
			<Typography variant="h4" className={classes.header}>
				{i18.loginHeader[language]}
			</Typography>
			<br />
			<Typography variant="body1">&nbsp;&nbsp;&nbsp;{i18.loginText[language]}</Typography>
			<Typography variant="body1">&nbsp;&nbsp;&nbsp;{i18.loginText2[language]}</Typography>
			<br />
			{codeData && [0,1,2].includes(codeData['remain']) && <Typography variant="body2">
				{`${i18.remainAttempts[language]}`.replace('%num%', codeData['remain'])}
			</Typography>}
			<br />
			{/*<Typography>{JSON.stringify({contact, code, haveAuthCode, codeData})}</Typography>*/}
			{/*<br />*/}
			<Paper elevation={0} className={classes.inputRoot}>
				<IconButton disabled={true}><PersonIcon /></IconButton>
				{' '}
				<TextField
					autoFocus={!haveAuthCode}
					margin="dense"
					id="contact"
					placeholder={i18.contactPlaceholder[language]}
					type="text"
					className={classes.inputContact}
					// fullWidth
					disabled={haveAuthCode || loading}
					onChange={e => dispatch({type: 'contact', payload: e.target.value})}
					onKeyPress={e => e.key === 'Enter' && getCode()}
					value={contact}
				/>
			</Paper>
			<br />

			{haveAuthCode ? null : <Button variant="contained"
									 onClick={getCode}
									 color="primary"
									 disabled={loading || countDown > 0 || `${contact}`.length === 0}>
				{i18.getCodeBtn[language]} {countDown && !loading ? `(${countDown})`:``}
			</Button>}
			<br />

			{haveAuthCode ? <>
				<Paper elevation={0} className={classes.inputRoot}>
					<IconButton disabled={true}><KeybIcon /></IconButton>
					{' '}
					<TextField
						autoFocus={haveAuthCode}
						margin="dense"
						id="code"
						placeholder={i18.codePlaceholder[language]}
						type="text"
						className={classes.inputCode}
						// fullWidth
						disabled={loading}
						onChange={e => dispatch({type: 'code', payload: e.target.value})}
						onKeyPress={e => e.key === 'Enter' && checkCode()}
						value={code} />
				</Paper>
				<br />

				<div className={classes.buttonContainer}>
					<Button variant="contained"
							onClick={cancelContact}
							disabled={loading}>
						{i18.cancelContactBtn[language]}
					</Button>
					{' '}
					<Button variant="contained"
							onClick={getCode}
							disabled={loading || countDown > 0 || codeData['remain'] <= 0}>
						{i18.getCodeBtnAgain[language]} {countDown && codeData['remain'] > 0 ? `(${countDown})`:``}
					</Button>
					{' '}
					<Button variant="contained"
							onClick={checkCode}
							color="primary"
							disabled={loading || `${contact}`.length === 0}>
						{i18.checkCodeBtn[language]}
					</Button>
				</div>
			</> : null}

			<br />
			<br />

		</Paper>
	);
}


class Login extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			haveAuthCode: false,
			open: true,
			contact1: '',
			contact: '',
			code: '',
		}
	}

	componentDidMount() {
		let { consumer: { cancelAction, coupon } } = this.props;
		if (!coupon) cancelAction();
	}

	getCode() {
		let { contact } = this.state;
		if (!contact) return;
		this.setState({ loading: true }, () => {
			let { consumer: { getAuthCode } } = this.props;
			getAuthCode({contact});
		});
	}

	checkCode() {
		let { contact, code } = this.state;
		if (!contact || !code) return;
		this.setState({ loading: true }, () => {

			let { consumer: { referral, checkAuthCode } } = this.props;

			checkAuthCode({ contact, code, referral });
		});
	}

	cancel() {
		let { consumer: { cancelAction, showLoginBox, haveAuthCode, coupon }, history } = this.props;
		haveAuthCode ? cancelAction() :
			coupon ? showLoginBox(false) : history.push("/");
	}

	onChange(e) {
		let { id, value } = e.target;
		this.setState({ [id]: value });
	}

	render() {
		let { consumer: { haveAuthCode, loading, language } = {} } = this.props;

		let { contact, code } = this.state;

		return (
			<Container maxWidth="sm" style={{backgroundColor: '#e0e0e0', padding: 40, marginTop: 100}}>
				<Typography variant="h4">
					{i18.loginHeader[language]}
				</Typography>
				<br />
				<Typography>{i18.loginText[language]}</Typography>

				<TextField
					autoFocus={!haveAuthCode}
					margin="dense"
					id="contact"
					label={i18.contactPlaceholder[language]}
					type="text"
					fullWidth
					disabled={haveAuthCode || loading}
					onChange={this.onChange.bind(this)}
					value={contact}
				/>

				{haveAuthCode ? <TextField
					autoFocus={haveAuthCode}
					margin="dense"
					id="code"
					label={i18.codePlaceholder[language]}
					type="text"
					disabled={loading}
					onChange={this.onChange.bind(this)}
					value={code}
				/> : null}

				<br />
				<br />
				<Button variant="contained" onClick={this.cancel.bind(this)} color="primary">{i18.cancelBtn[language]}</Button>
				{' '}
				{!haveAuthCode ? <Button variant="contained" onClick={this.getCode.bind(this)} color="primary"
										 disabled={loading}>{i18.getCodeBtn[language]}</Button> : null}
				{' '}
				{haveAuthCode ? <Button variant="contained" onClick={this.checkCode.bind(this)} color="primary"
										disabled={loading}>{i18.checkCodeBtn[language]}</Button> : null}

			</Container>
		);
	}
}

export default withConsumer(withRouter(Login2));
