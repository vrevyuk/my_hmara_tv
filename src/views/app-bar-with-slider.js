/**
 * Created by Vitaly Revyuk on 6/6/19.
 */
import React from 'react';
import { withConsumer } from '../context';
import { withRouter } from "react-router";
import i18d from '../depend_on_portal_dictionary';

import ToggleLanguage from './toggle-language';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';

import MenuIcon from '@material-ui/icons/Menu';

class ApplicationBar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	handleClose(e) {
		let { id } = e.currentTarget;

		this.setState({ anchorEl1: null, anchorEl2: null }, () => {

			let { consumer: { logOut, setLanguage } = {}, history } = this.props;

			switch (id) {
				case 'uk':
				case 'en':
				case 'ru': return setLanguage(id);
				case 'home': return history.push('/');
				case 'login': return history.push('/login');
				case 'contacts': return history.push('/contacts');
				case 'make-payment': return history.push('/make-payment');
				case 'packets': return history.push('/packets');
				case 'messages': return history.push('/messages');
				case 'transactions': return history.push('/transactions');
				case 'logout': return logOut();
				default: break;
			}
		});
	}

	handleClick1(e) {
		this.setState({ anchorEl1: e.target });
	}

	handleClick2(e) {
		this.setState({ anchorEl2: e.target });
	}

	render() {

		let { consumer: { accessToken, toggleDrawer, language, contacts = [], device } } = this.props;


		let toolbarStyle = {
			flexGrow: 1,
			backgroundColor: 'white',
			color: '#101010'
		};

		return 	<AppBar position="static">
			<Toolbar style={toolbarStyle}>
				{device.isMobile ? (
						<IconButton onClick={toggleDrawer} edge="start" color="inherit" aria-label="Menu1">
							<MenuIcon />
						</IconButton>
					) : null}

				<Typography variant="h6">{i18d().appTitle[language]}</Typography>

				<div style={{ flexGrow: 1 }}/>

				{accessToken ? <Typography variant="h6">{contacts[0]}</Typography> : null}

				<Typography variant="h6"><ToggleLanguage variant="h6" /></Typography>
			</Toolbar>
		</AppBar>


	}
}

export default withConsumer(withRouter(ApplicationBar));