/**
 * Created by Vitaly Revyuk on 6/6/19.
 */

import React from 'react';
import { withRouter } from "react-router";
import { withConsumer } from '../context';
import i18 from '../dictionary';
import i18d from '../depend_on_portal_dictionary';

import { createStyles } from '@material-ui/core'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Divider from '@material-ui/core/Divider'
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Toolbar from '@material-ui/core/Toolbar';

import { NavigateBefore, Person, ThumbsUpDownOutlined, Email, LiveTv, PlaylistPlayOutlined } from "@material-ui/icons";

import { HomeSVG, SubscriptionsSVG, PaymentSVG, TransactionsSVG, MessagesSVG, CameraSVG, ContactsSVG,
	PromoSVG, ExitSVG } from '../assets';

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';
import Paper from "@material-ui/core/Paper";

const getMenuItems = function (language) {
	return [
		{ id: 'home', icon: <HomeSVG />, title: i18.menuHome[language], pathname: '/'  },
		{ id: 'packets', icon: <SubscriptionsSVG />, title: i18.menuPackets[language], pathname: '/packets' },
		{ id: 'make-payment', icon: <PaymentSVG />, title: i18.menuMakePayment[language], pathname: '/make-payment' },
		{ id: 'transactions', icon: <TransactionsSVG />, title: i18.balance[language], pathname: '/transactions' },
		{ id: 'messages', icon: <MessagesSVG />, title: i18.menuMessages[language], pathname: '/messages' },
		{ id: 'cameras', icon: <CameraSVG />, title: i18.menuCameras[language], pathname: '/cameras' },
		{ id: 'contacts', icon: <ContactsSVG />, title: i18.menuContacts[language], pathname: '/contacts' },
		{ id: 'promo', icon: <PromoSVG />, title: i18.menuPromo[language], pathname: '/promo' },
		// { id: 'callback', icon: <FeedbackOutlined />, title: i18.menuFeedback[language], pathname: '/feedback' },
		{ id: 'player', icon: <LiveTv />, title: i18.menuPlayer[language], pathname: '' },
		{ id: 'playlist', icon: <PlaylistPlayOutlined />, title: i18.menuPlayList[language], pathname: '/playlist' },
		{ id: 'referral', icon: <ThumbsUpDownOutlined />, title: i18.menuReferral[language], pathname: '/referral' },
	]
};

const style = createStyles({
	root: {
		display: 'flex',
		flexGrow: 1,
		flexDirection: 'column',
		alignItems: 'stretch',
		justifyContent: 'space-between',
	},

	menuList: {
		padding: 5
	},

	contacts: {
		display: 'flex',
		justifyContent: 'space-around',
	},

	toolbar: {
		padding: 5,
		flexGrow: 1,
		color: '#101010'
	},

});

class MenuList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			uk, en, ru,
		}
	}

	handleClick(item) {
		let { pathname } = item;
		let { consumer: { toggleDrawer, device, refreshToken, portal }, history } = this.props;

		if (item.id === 'player') {
			const server_name = /hmara/g.test(portal) ? 'pc.hmara.tv'
				: /vivat/g.test(portal) ? 'pc.vivat.live'
				: 'google.com';
			return window.open(`http://${server_name}/?refreshToken=${refreshToken}`);
		} else {
			device.isMobile && toggleDrawer(); history.push(pathname);
		}
	}

	handleClickExit() {
		let { consumer: { accessToken, logOut, language }, history } = this.props;
		accessToken ? window.confirm(i18.doYouWantToChangeUser[language]) && logOut() : history.push('/login');
	}

	render() {

		let { consumer: { accessToken, toggleDrawer, language, device }, location: { pathname } } = this.props;
		moment.locale(language || 'uk');

		return <div style={style.root}>
			<div style={style.menuList}>
				{device.isMobile || device.isTablet ? (
					<React.Fragment>
						<Toolbar style={style.toolbar}>
							<IconButton onClick={toggleDrawer}>
								<NavigateBefore />
							</IconButton>
							<Typography variant="h6">{i18d().appTitle[language]}</Typography>
						</Toolbar>
						<Divider />
					</React.Fragment>
				) : null}

				<List>
					{
						getMenuItems(language, accessToken).map( (item = {}, key) => (
							<ListItem button
									  key={key}
									  id={item.id}
									  onClick={this.handleClick.bind(this, item)}
									  selected={item.pathname === pathname}
									  style={{
										  borderLeftWidth: 3,
										  borderLeftStyle: 'solid',
										  borderLeftColor: item.pathname === pathname ? 'purple':'transparent',
									  }} >
								<ListItemIcon>{item.icon}</ListItemIcon>
								<ListItemText primary={item.title} />
							</ListItem>
						))
					}

					<ListItem  />

					<ListItem button onClick={this.handleClickExit.bind(this)} >
						<ListItemIcon>{accessToken ? <ExitSVG /> : <Person />}</ListItemIcon>
						<ListItemText primary={accessToken ? i18.menuLogout[language] : i18.menuLogin[language]} />
					</ListItem>
				</List>
			</div>

			<div style={style.contacts}>
				<IconButton href="viber://chat/?number=+380730190374">
					<img alt="viber" src='https://hmara.tv/static/img/viber.png'/></IconButton>
				{' '}
				<IconButton href="https://www.facebook.com/HmaraTelevision" target="_new">
					<img alt="facebook" src='https://hmara.tv/static/img/fb.png'/></IconButton>
				{' '}
				<IconButton href="mailto:info@hmara.tv"><Email /></IconButton>
			</div>
		</div>;
	}
}

export default withConsumer(withRouter(MenuList));
