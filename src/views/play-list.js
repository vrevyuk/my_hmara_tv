/**
 * Created by Vitaly Revyuk on 6/5/19.
 */
import React from 'react';
import { withRouter } from 'react-router';
import { withConsumer } from '../context';
import i18 from '../dictionary';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from "@material-ui/core/Button";

import PlayListDevicesDialog from './play-list-devices-dialog';

class PlayList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			showDevicesDialog: false
		};
	}

	toggleDevicesDialog() {
		console.log(this.state.showDevicesDialog);
		this.setState({ showDevicesDialog: !this.state.showDevicesDialog });
	}

	render() {
		let { consumer: { loading, language, contentTypes, playlist } } = this.props;
		let { showDevicesDialog } = this.state;

		return <div style={{ padding: 20 }}>
			<Paper style={{ padding: 20 }}>

				<div><Typography>&nbsp;&nbsp;&nbsp;{i18.playListsDescription1[language]}</Typography></div>
				<br/>
				<div><Typography>&nbsp;&nbsp;&nbsp;{i18.playListsDescription2[language]}</Typography></div>
				<br/>
				<div><Typography>&nbsp;&nbsp;&nbsp;<div dangerouslySetInnerHTML={{__html: playlist}}></div></Typography></div>
				<br/>
				<Button variant="outlined"
						onClick={this.toggleDevicesDialog.bind(this)}
						color="primary"
						disabled={loading}>
					{i18.activeDevicesBtn[language]}
				</Button>
			</Paper>
			<Typography>{loading && !showDevicesDialog ? i18.loading[language] : ''}</Typography>

			{showDevicesDialog && <PlayListDevicesDialog toggleDialog={this.toggleDevicesDialog.bind(this)} />}
		</div>;
	}
}

export default withConsumer(withRouter(PlayList));
