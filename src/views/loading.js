/**
 * Created by Vitaly Revyuk on 6/5/19.
 */
import React from 'react';
import { withConsumer } from '../context';
import i18 from '../dictionary';

import Typography from "@material-ui/core/Typography";

class Loading extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}
	render() {
		let { consumer: { loading, language } = {} } = this.props;

		return loading ? <div style={{ padding: 10 }}>
			<Typography>{i18.loading[language]}</Typography>
		</div> : null;
	}
}

export default withConsumer(Loading);