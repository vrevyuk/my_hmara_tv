/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import React from 'react';
import { withConsumer } from '../context';

import i18 from '../dictionary';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const TryingRefreshAuthAlert = (props) => {

	let { consumer: { language } } = props;

	return (<Dialog open={true}>
		<DialogTitle id="form-dialog-title">{i18.loginHeader[language]}</DialogTitle>
		<DialogContent>
			<DialogContentText>
				{i18.tryingRestoreAuth[language]}
			</DialogContentText>
		</DialogContent>
	</Dialog>);
};

export default withConsumer(TryingRefreshAuthAlert);