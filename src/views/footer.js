/**
 * Created by Vitaly Revyuk on 6/6/19.
 */

import React from 'react';
import { withRouter } from "react-router";
import { withConsumer } from '../context';
import i18 from '../dictionary';
import i18d from '../depend_on_portal_dictionary';

import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';

class Footer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			uk, en, ru,
		}
	}

	render() {
		let {consumer: {language, device:{osName}}} = this.props;
		moment.locale(language || 'uk');

		return <Grid container spacing={0}>
			<Grid item  xs={12} sm={12} md={12}  lg={12} xl={12}>
				<Paper>
					<br />
					<Typography style={{ textAlign: 'center' }}><b>{i18d().appTitle[language]}</b> © 2010 – {moment().format('YYYY')}. Все права защищены. ver. {process.env.REACT_APP_VERSION} on {osName}</Typography>
					<br />
				</Paper>
			</Grid>
		</Grid>;
	}
}

export default withConsumer(withRouter(Footer));


