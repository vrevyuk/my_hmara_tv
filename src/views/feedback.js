/**
 * Created by Vitaly Revyuk on 6/5/19.
 */
import React from 'react';
import { withRouter } from 'react-router';
import { withConsumer } from '../context';
// import i18 from '../dictionary';

import Button from "@material-ui/core/Button";

import { Phone, Email } from '@material-ui/icons';


class Feedback extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	render() {
		// let { consumer: { accessToken } } = this.props;

		return <div>
			<Button href="tel:+380443379428"><Phone /> +380443379428</Button>
			{' '}
			<Button href="tel:+380970111959"><Phone /> +380970111959</Button>
			{' '}
			<Button href="tel:+380669515079"><Phone /> +380669515079</Button>
			{' '}
			<Button href="tel:+380970111959"><Phone /> +380970111959</Button>
			{' '}
			<Button href="viber://chat/?number=+380730190374">
				<img alt="viber" src='http://hmara.tv/static/img/viber.png'/></Button>
			{' '}
			<Button href="https://www.facebook.com/HmaraTelevision" target="_new">
				<img alt="facebook" src='http://hmara.tv/static/img/fb.png'/></Button>
			{' '}
			<Button href="mailto:info@hmara.tv"><Email /></Button>
			{' '}
		</div>;
	}
}

export default withConsumer(withRouter(Feedback));