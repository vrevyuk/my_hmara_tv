/**
 * Created by Vitaly Revyuk on 6/6/19.
 */
import React from 'react';
import { withConsumer } from '../context';
import i18 from '../dictionary';
import Loading from './loading';

import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';
import Select from '@material-ui/core/Select';
import MenuItem from "@material-ui/core/MenuItem";
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';

import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import AutoPaymentIcon from '@material-ui/icons/Schedule';

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';

class MakePayment extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			ignoreExistsAutoPayments: false,
			uk, en, ru,
			infoDialog: false,
			infoDialogTitle: '',
			infoDialogText: '',
			activeStep: 0,
			steps: ['first','second','third','fourth'],
			acquiring: -1,
			amount: 0,
			currency: 'UAH',

			prevAmount: 0,
			useDiscount: null,

			amountText: -1,
			allowAutoPayment: true,
		}
	}

	componentDidMount() {
		let { consumer: { getAcquiringList } } = this.props;
		getAcquiringList();
	}

	componentDidUpdate(prevProps, prevState, snapShot) {
		let { consumer: { language: prevLanguage, getAcquiringList } } = this.props;
		let { consumer: { language: nextLanguage } } = prevProps;
		if (prevLanguage !== nextLanguage) {
			getAcquiringList();
		}
	}

	handleReset() {
		this.setState({
			activeStep: 0,
			infoDialog: false,
			infoDialogTitle: '',
			infoDialogText: '',
			acquiring: -1,
			amount: 0,
			currency: 'UAH',
			prevAmount: 0,
			useDiscount: null,
			amountText: -1,
			allowAutoPayment: true,
		});
	}

	handleNext() {
		let { activeStep } = this.state;
		this.setState({ activeStep: activeStep + 1 });
	}

	handleBack() {
		let { activeStep } = this.state;
		this.setState({ activeStep: activeStep - 1 });
	}

	applyAcquiring(key) {
		let {activeStep} = this.state;
		let {consumer: {currency: userCurrency = 'UAH', acquiringList = []}} = this.props;
		const acquiring = acquiringList[key];
		if (acquiring) {
			if (!acquiring.uri) {
				return this.toggleInfoDialog(acquiring);
			}
		}
		this.setState({ acquiring: key, currency: userCurrency, activeStep: key === -1 ? activeStep - 1 : activeStep + 1 });
	}

	changeAmountText(e) {
		let { value: amountText } = e.target;
		this.setState({ amountText });
	}

	applyAmountText(e) {
		let { amountText, amount, activeStep } = this.state;
		let { consumer: { recommendedSum } } = this.props;
		if (amountText === -1) amountText = recommendedSum;
		amount = parseFloat(amountText || amount || recommendedSum);
		!isNaN(amount) && amount > 0 && this.setState({ amount, amountText: amount, activeStep: activeStep + 1 });
	}

	applyDiscount(useDiscount) {
		let {consumer: { recommendedSum}} = this.props;
		let {activeStep, amount, prevAmount} = this.state;
		this.setState({
			useDiscount,
			activeStep: activeStep + 1,
			prevAmount: useDiscount ? amount : 0,
			amount: useDiscount ? (recommendedSum * 10) : (prevAmount || amount) }, () => {

			let { consumer: { contacts = [], acquiringList = [], preparePayment } } = this.props;
			let { acquiring, amount, currency, allowAutoPayment } = this.state;
			let { uri } = acquiringList[acquiring] || {};

			setImmediate(()=>preparePayment({
				acquiringUri: uri,
				customer: contacts[0],
				amount,
				currency,
				autoPayment: allowAutoPayment,
			}));

		});
	}

	toggleInfoDialog(acquiring = {}) {
		let {description: {portal, site, oldPortal} = {}} = acquiring;
		let {infoDialog} = this.state;
		this.setState({
			infoDialog: !infoDialog,
			infoDialogTitle: acquiring.title,
			infoDialogText: site
		});
	}

	toggleAutoPayment() {
		let {allowAutoPayment} = this.state;
		this.setState({allowAutoPayment: !allowAutoPayment});
	}

	toggleCurrency(e) {
		let { value } = e.target;
		this.setState({currency: value});
	}

	toggleIgnoreAutoPayment() {
		let {ignoreExistsAutoPayments} = this.state;
		this.setState({ignoreExistsAutoPayments: !ignoreExistsAutoPayments});
	}

	removeAutoPayment() {
		this.props.consumer.removeUserAutoPayments && this.props.consumer.removeUserAutoPayments();
	}

	getStepContent(step, key) {

		let {
			consumer: {
				loading, language = 'uk', recommendedSum, acquiringList = [], payment
			}
		} = this.props;
		let {
			acquiring, amount, currency, amountText, useDiscount, steps, activeStep, allowAutoPayment
		} = this.state;
		let { action, inputs = {} } = payment || {};

		let { currencies = [] } = acquiringList[acquiring] || {};

		switch (step) {
			case 'first': return <Step key={key}>
				<StepLabel>{acquiringList[acquiring] ? acquiringList[acquiring].title : i18.paymentStepperFirst[language]}</StepLabel>
				<StepContent>
					<List>{acquiringList.map( (acquiring, key) => (
						<React.Fragment key={key}>
							<ListItem button onClick={this.applyAcquiring.bind(this, key)}
									  variant="contained"
									  color="primary">
								<ListItemText>
									{acquiring.title}
									{' '}
									{acquiring.autoPayment === 1 ? <Tooltip title={i18.enabledAutoPayment[language]}>
										<AutoPaymentIcon style={{color: 'green'}}/>
									</Tooltip>: null}
								</ListItemText>
								{' '}
								<Typography>{acquiring['currencies'].join(', ')}{' '}</Typography>
								<ListItemSecondaryAction>
								</ListItemSecondaryAction>
							</ListItem>
							<Divider />
						</React.Fragment>
					))}</List>
				</StepContent>
			</Step>;

			case 'second': return <Step key={key}>
				<StepLabel>{amount
					? `${currency}, ${amount.toFixed(2)}`
					: `${i18.paymentStepperSecond[language]} ${recommendedSum || 0} ${currency.toLocaleLowerCase()}`}</StepLabel>
				<StepContent>
					<br/>
					<FormControl>
						<InputLabel htmlFor="adornment-amount">{i18.paymentAmount[language]}</InputLabel>
						<Input
							id="adornment-amount"
							value={amountText === -1 ? recommendedSum : amountText}
							onChange={this.changeAmountText.bind(this)}
							// startAdornment={<InputAdornment position="start">{currency}</InputAdornment>}
						/>
					</FormControl>
					{' '}
					<FormControl>
						<InputLabel htmlFor="select-currency">Currency</InputLabel>
						<Select
							value={currency}
							onChange={this.toggleCurrency.bind(this)}
							inputProps={{
								name: 'select-currency',
								id: 'currency',
							}}
						>
							{currencies.map( (curr, key) => <MenuItem key={key} value={curr}>{curr}</MenuItem>)}
						</Select>
					</FormControl>
					<br />
					<br />
					{acquiringList[acquiring] && acquiringList[acquiring]['allowedAutoPayment'] ? <div>
						<Typography>
							<Checkbox checked={allowAutoPayment} onChange={this.toggleAutoPayment.bind(this)}
							/>
							{i18.addAutoPaymentCheckbox[language]}
						</Typography>
					</div> : null}
					<p>
						<Button onClick={this.applyAcquiring.bind(this, -1)} variant="outlined" color="secondary">{i18.paymentBackBtn[language]}</Button>
						{' '}
						<Button onClick={this.applyAmountText.bind(this)} variant="outlined" color="primary">{i18.paymentNextBtn[language]}</Button>
					</p>
				</StepContent>
			</Step>;

			case 'third': return <Step key={key}>
				<StepLabel>{useDiscount === null ? i18.paymentStepperThird[language] :
					useDiscount ? i18.paymentIWantToUseAction[language] : i18.paymentIDontWantToUse[language]}</StepLabel>
				<StepContent>
					<p>{i18.paymentTenPlusTwo[language]}</p>
					<Button onClick={this.handleBack.bind(this)} variant="outlined" color="secondary">{i18.paymentBackBtn[language]}</Button>
					{' '}
					<Button onClick={this.applyDiscount.bind(this, true)} variant="outlined" color="primary">{i18.paymentYesBtn[language]}</Button>
					{' '}
					<Button onClick={this.applyDiscount.bind(this, false)} variant="outlined" color="primary">{i18.paymentNoBtn[language]}</Button>
				</StepContent>
			</Step>;

			case 'fourth': return <Step key={key}>
				<StepLabel>{activeStep > steps.length - 1 ? i18.paymentCompleteHeader[language] : i18.paymentStepperFourth[language]}</StepLabel>
				<StepContent>
					{loading ? (
						<Typography>{i18.paymentPreparePayment[language]}</Typography>
					) : payment ? (
						<form method="post"
							  target="_new"
							  onSubmit={this.handleNext.bind(this)}
							  action={action}
						>
							{
								Object.keys(inputs).map(key => {
									return <input key={key} type="hidden" name={key} value={inputs[key]}/>
								})
							}
							<br />
							<Typography>{i18.paymentMake[language]}</Typography>
							<br />
							<Button onClick={this.handleBack.bind(this)} variant="outlined" color="secondary">{i18.paymentBackBtn[language]}</Button>
							{' '}
							<Button type="submit" variant="outlined" color="primary">{i18.paymentYesBtn[language]}</Button>
						</form>
					) : (
						<React.Fragment>
							<Button onClick={this.handleBack.bind(this)} variant="outlined" color="secondary">{i18.paymentBackBtn[language]}</Button>
							{' '}
							<p>{i18.paymentError[language]}</p>
						</React.Fragment>
					)}
				</StepContent>
			</Step>;

			default: break;
		}
	}

	render() {
		let {
			consumer: {
				language = 'uk', currency: userCurrency = 'UAH', recommendedSum, acquiringList = []
			}
		} = this.props;
		let {
			activeStep, steps, infoDialog, infoDialogTitle = '', infoDialogText = '', ignoreExistsAutoPayments
		} = this.state;
		moment.locale(language || 'uk');
		let acquiringWhereEnabledAutoPayment = acquiringList.filter(a => a.autoPayment === 1);

		return <div style={{ padding: 10 }}>
			<Loading />
			<p />
			<div>
				{acquiringWhereEnabledAutoPayment.length > 0 && !ignoreExistsAutoPayments ? (
					<div>
						<Typography>{i18.alreadyEnabledAutoPayment[language]}</Typography>
						<br />
						<List>
							{acquiringWhereEnabledAutoPayment.map( (a, key) => <ListItem key={key}>
								<Typography>
									{a.title}: {recommendedSum} {userCurrency}
								</Typography>
							</ListItem>)}
						</List>
						<br />
						<Typography>{i18.removeAutoPayment[language]}</Typography>
						<br/>
						<div>
							<Button onClick={this.removeAutoPayment.bind(this)}
									variant="contained"
									color="primary">
								{i18.paymentYesBtn[language]}
							</Button>
							{' '}
							<Button onClick={this.toggleIgnoreAutoPayment.bind(this)}
									variant="contained"
									color="secondary">
								{i18.ignoreAutoPayment[language]}
							</Button>
						</div>
					</div>
				) : (
					<Stepper activeStep={activeStep} orientation="vertical">
						{steps.map(this.getStepContent.bind(this))}
					</Stepper>
				)}
				<br />
				{activeStep > steps.length - 1 ? <div style={{ padding: 20 }}>
						<Typography variant="h5" component="h3">
							{i18.paymentCompleteHeader[language]}
						</Typography>
						<br />
						<Typography component="p">
							{i18.paymentCompleteText[language]}
						</Typography>
						<br />
						<Button onClick={this.handleReset.bind(this)} variant="contained" color="secondary">{i18.paymentResetBtn[language]}</Button>
					</div> : null}
			</div>

			<Dialog
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
				open={infoDialog}
				onClose={this.toggleInfoDialog.bind(this)}
			>
				<DialogTitle id="scroll-dialog-title">{infoDialogTitle}</DialogTitle>
				<DialogContent>
					<DialogContentText dangerouslySetInnerHTML={{__html: infoDialogText}} />
				</DialogContent>
				<DialogActions>
					<Button onClick={this.toggleInfoDialog.bind(this)} color="primary" variant='outlined'>
						{i18.paymentBackBtn[language]}
					</Button>
				</DialogActions>
			</Dialog>
		</div>;
	}
}

export default withConsumer(MakePayment);