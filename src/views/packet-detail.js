/**
 * Created by Vitaly Revyuk on 6/6/19.
 */

import React from 'react';
import { withRouter } from "react-router";
import { withConsumer } from '../context';
// import i18 from '../dictionary';
import { Loading } from './index';

import { LazyLoadImage } from 'react-lazy-load-image-component';

import Typography from "@material-ui/core/Typography";

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';

class PacketDetail extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			uk, en, ru,
		}
	}

	componentDidMount() {
		let { consumer: { getPacketDetail }, match: { params: { id } } } = this.props;
		getPacketDetail(id);
	}

	render() {
		let { consumer: { loading, language, packetDetail = [], setting: { imagesServer } = {} } } = this.props;
		moment.locale(language || 'uk');

		return <div style={{ padding: 10 }}>
			{loading ? <Loading /> : null}

			<Typography variant='h5'>Список ТБ каналів в пакеті</Typography>

			<p />
			{!loading && packetDetail.length === 0 ? <Typography variant="subtitle1">В цьому пакеті немає ТБ каналів</Typography> : null}
			{
				packetDetail.map(
					(content, key) => <LazyLoadImage
						key={key}
						src={`${imagesServer}/${content.image}`}
						alt={content.title}
						height={100}
						width={100} />
					)
			}
		</div>;
	}
}

export default withConsumer(withRouter(PacketDetail));


