/**
 * Created by Vitaly Revyuk on 6/4/19.
 */

import React from 'react';
import { isBrowser, isMobile, osName } from 'react-device-detect';
import * as Actions from './actions';
import reducer from './reducer';

const Context = React.createContext();

class AppProvider extends React.Component {
	constructor(props) {
		super(props);

		const storedLang = localStorage.getItem('language');
		const languages = ['uk','en','ru'];
		const [_, queryStr] = window.location.href.split('?');
		const query = `${queryStr}`.split('&').reduce((acc, item) => {
			const [key, value] = `${item}`.split('=');
			return {
				...acc,
				[key]: value
			};
		}, {});

		const lang = languages.includes(query['language']) ? query['language']
			: languages.includes(storedLang) ? storedLang : 'uk';

		localStorage.setItem('language', lang);

		this.state = {
			device: { isBrowser, osName, isMobile, protocol: process.env.NODE_ENV === "development" ? 'http':'https' },
			loading: false,
			haveAuthCode: false,
			codeData: {},
			accessToken: false,
			tryingRestoreAuth: false,
			languages,
			language: lang,
			drawerOpened: false,
			userMessages: [],
			transactions: [],
			packets: [],
			packetDetail: [],
			contacts: [],
			cameras: [],
			addContactDialog: false,
			payment: null,
			applyCouponSuccess: false,
			isLoginBoxShows: false,

			contentTypes: [],
			activeDevices: [],
		};
	}

	mapDispatchToActions(actions) {
		let { state = {} } = this;
		return Object.keys(actions)
			.reduce( (acc, actionFnName) => ({
				...acc,
				[actionFnName]: (...props) => actions[actionFnName](...props)( state, (action) => this.setState(reducer(state, action))),
			}), {});
	}

	render() {
		return <Context.Provider value={{
			...this.state,
			...this.mapDispatchToActions(Actions),
		}} >
			{this.props.children}
		</Context.Provider>
	}
}

const withConsumer = function (Component) {
	return (props) => <Context.Consumer>{
		(state) => <Component {...props} consumer={state} />
	}</Context.Consumer>;
};

export {
	AppProvider,
	withConsumer
}