/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import axios from 'axios';
import i18 from '../dictionary';

const getUserPackets = (packetId) => {
	return async (state, dispatch) => {

		dispatch({ type: 'I_AM_BUSY' });
		let { accessToken, language } = state;

		try {

			let { data: { packets } } = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/unsubscribe?packetId=${packetId}&language=${language}`,
			});

			dispatch({
				type: 'USER_PACKETS',
				payload: packets
			});

		} catch (e) {
			dispatch({ type: 'I_AM_NOT_BUSY' });

			let message = e.response ? e.response.data : e.message;
			alert(i18[message] ? (i18[message][state.language] || message) : message);
		}
	}
};

export default getUserPackets;