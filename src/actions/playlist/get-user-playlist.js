import axios from 'axios';

const getUserPlayList = ({ playListIndex }) => {
	return async (state, dispatch) => {

		dispatch({
			type: 'USER_PLAYLIST',
			payload: { playListContent: [], playListIndex, loading: true }
		});

		try {
			let { playLists, playListContentLimit: limit, playListContentOffset: offset, accessToken, language } = state;
			let playList = playLists[playListIndex];

			let { data: playListContent } = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/site/satmarket/${playList.id}?language=${language}`,
				params: { limit, offset },
			});

			dispatch({
				type: 'USER_PLAYLIST',
				payload: { playListContent, playListIndex, loading: false }
			});

		} catch (e) {
			dispatch({
				type: 'USER_PLAYLIST',
				payload: { playListContent: [], playListIndex, loading: false }
			});
			alert(e.response ? e.response.data : e.message);
		}
	}
};

export default getUserPlayList;