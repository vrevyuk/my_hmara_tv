import axios from 'axios';

const deleteActiveDevice = (sessionId) => {
	return async (state, dispatch) => {

		let {accessToken} = state;
		if (!accessToken) return;

		dispatch({
			type: 'USER_PLAYLIST',
			payload: { loading: true }
		});

		try {

			let { data: activeDevices } = await axios({
				method: 'delete',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/satmarket/devices/${sessionId}`,
			});

			dispatch({
				type: 'USER_PLAYLIST',
				payload: { loading: false, activeDevices }
			});

		} catch (e) {
			dispatch({
				type: 'USER_PLAYLIST',
				payload: { loading: false }
			});
			alert(e.response ? e.response.data : e.message);
		}
	}
};

export default deleteActiveDevice;