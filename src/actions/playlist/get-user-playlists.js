import axios from 'axios';

const getUserPlayLists = ({ listType }) => {
	return async (state, dispatch) => {

		dispatch({
			type: 'USER_PLAYLIST',
			payload: { playLists: [], playListContent: [], playListIndex: 0, loading: true }
		});

		try {

			let { accessToken, language, limit, offset } = state;

			let { data: playLists } = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/site/satmarket`,
				params: { listType, language }
			});

			let playList = playLists[0];

			let { data: playListContent } = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/site/satmarket/${playList.id}`,
				params: { limit, offset, language },
			});

			console.log('playLists', playLists.length);
			console.log('playListContent', playListContent.length);
			dispatch({
				type: 'USER_PLAYLIST',
				payload: { playLists, playListContent, loading: false }
			});

		} catch (e) {
			dispatch({
				type: 'USER_PLAYLIST',
				payload: { playLists: [], loading: false }
			});
			alert(e.message);
		}

	}
};

export default getUserPlayLists;