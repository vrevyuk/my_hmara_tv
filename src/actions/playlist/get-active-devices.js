import axios from 'axios';

const getActiveDevices = () => {
	return async (state, dispatch) => {

		let {accessToken, language} = state;
		if (!accessToken) return;

		dispatch({
			type: 'USER_PLAYLIST',
			payload: { loading: true }
		});

		try {

			let { data: activeDevices } = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/satmarket/devices?language=${language}`,
			});

			dispatch({
				type: 'USER_PLAYLIST',
				payload: { loading: false, activeDevices }
			});

		} catch (e) {
			dispatch({
				type: 'USER_PLAYLIST',
				payload: { loading: false }
			});
			alert(e.response ? e.response.data : e.message);
		}
	}
};

export default getActiveDevices;