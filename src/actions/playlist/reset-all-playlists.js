import axios from 'axios';

const resetAllPlayLists = () => {
	return async (state, dispatch) => {

		dispatch({
			type: 'USER_PLAYLIST',
			payload: { loading: true }
		});

		try {

			let { accessToken, language } = state;

			let {data: {playlist} = {}} = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/satmarket/reset?language=${language}`,
			});

			// console.log(playlist);
			dispatch({
				type: 'USER_PLAYLIST',
				payload: {loading: false, playlist: `<span style="color: red;">NEW >>>> </span>${playlist}<span style="color: red;"> <<<< NEW</span>`}
			});

			alert('Successfully');

		} catch (e) {
			dispatch({
				type: 'USER_PLAYLIST',
				payload: { loading: false }
			});
			alert(e.response ? e.response.data : e.message);
		}
	}
};

export default resetAllPlayLists;