import axios from 'axios';
import fileDownload from 'js-file-download';

const downloadUserPlayList = () => {
	return async (state, dispatch) => {

		dispatch({
			type: 'USER_PLAYLIST',
			payload: { loading: true }
		});

		try {
			let { playLists, playListIndex, playListContentLimit: limit, playListContentOffset: offset, accessToken, language } = state;
			let playList = playLists[playListIndex];

			let { data: playListContentBlob } = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/site/satmarket/${playList.id}?language=${language}`,
				params: { limit, offset, downloadable: true  },
				responseType: 'blob',
			});

			fileDownload(playListContentBlob, 'hmara_tv_playlist.m3u');

			dispatch({
				type: 'USER_PLAYLIST',
				payload: { loading: false }
			});


		} catch (e) {
			dispatch({
				type: 'USER_PLAYLIST',
				payload: { loading: false }
			});
			alert(e.response ? e.response.data : e.message);
		}
	}
};

export default downloadUserPlayList;