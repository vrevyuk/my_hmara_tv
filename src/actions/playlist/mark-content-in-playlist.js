import axios from 'axios';

const createUserPlayList = (content = {}) => {
	return async (state, dispatch) => {

		let { accessToken, language, playLists, playListContent, playListIndex } = state;
		let playList = playLists[playListIndex];
		let { id: contentId, included } = content;

		try {

			dispatch({
				type: 'USER_PLAYLIST',
				payload: {
					loading: true,
					playListContent: playListContent
						.map( content => content.id === contentId ? { ...content, included: !included } : content )
				}
			});

			if (playList) {

				await axios({
					method: included ? 'delete': 'post',
					headers: {
						'Authorization': `Bearer ${accessToken}`
					},
					url: `/api/site/playListContent`,
					data: { contentId, playListId: playList.id },
				});

				dispatch({
					type: 'USER_PLAYLIST',
					payload: {
						loading: false,
						playListContent: playListContent
							.map( content => content.id === contentId ? { ...content, included: !included } : content )
					}
				});
			}

		} catch (e) {
			dispatch({
				type: 'USER_PLAYLIST',
				payload: {
					playListContent
				}
			});
			alert(e.message);
		}
	}
};

export default createUserPlayList;