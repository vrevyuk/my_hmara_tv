import axios from 'axios';

const getContentTypesPlaylist = () => {
	return async (state, dispatch) => {

		dispatch({
			type: 'USER_PLAYLIST',
			payload: { contentTypes: [], loading: true }
		});

		try {

			let { accessToken, language } = state;

			let { data: contentTypes } = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/satmarket?language=${language}`,
			});

			dispatch({
				type: 'USER_PLAYLIST',
				payload: { loading: false, contentTypes }
			});

		} catch (e) {
			dispatch({
				type: 'USER_PLAYLIST',
				payload: { contentTypes: [], loading: false }
			});
			alert(e.response ? e.response.data : e.message);
		}
	}
};

export default getContentTypesPlaylist;