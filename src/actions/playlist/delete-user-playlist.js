import axios from 'axios';

const deleteUserPlayList = () => {
	return async (state, dispatch) => {

		dispatch({ type: 'USER_PLAYLIST', payload: { loading: true } });

		try {
			let { accessToken, language, playLists, playListIndex, limit, offset } = state;

			let playList = playLists[playListIndex];

			if (playList) {
				await axios({
					method: 'delete',
					headers: {
						'Authorization': `Bearer ${accessToken}`
					},
					url: `/api/site/satmarket/${playList.id}?language=${language}`,
				});

				playLists = playLists.filter( pl => pl.id !== playList.id );

				playListIndex += 1;
				if (playListIndex > playLists.length - 1) playListIndex = playLists.length - 1;
				if (playListIndex < 0) playListIndex = 0;

				playList = playLists[playListIndex];

				if (playList) {
					let { data: playListContent } = await axios({
						method: 'get',
						headers: {
							'Authorization': `Bearer ${accessToken}`
						},
						url: `/api/site/satmarket/${playList.id}?language=${language}`,
						params: { limit, offset },
					});

					dispatch({
						type: 'USER_PLAYLIST',
						payload: {
							playLists, playListContent, playListIndex, loading: false
						}
					});
				} else {
					dispatch({
						type: 'USER_PLAYLIST',
						payload: {
							playLists, playListContent: [], playListIndex, loading: false
						}
					});
				}
			}

		} catch (e) {
			dispatch({ type: 'USER_PLAYLIST', payload: { loading: false, playListContent: [] } });
			alert(e.message);
		}
	}
};

export default deleteUserPlayList;