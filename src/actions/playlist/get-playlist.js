import axios from 'axios';
import fileDownload from "js-file-download";

const getPlaylist = (type = {}) => {
	return async (state, dispatch) => {

		dispatch({
			type: 'USER_PLAYLIST',
			payload: { loading: true }
		});

		try {

			let { accessToken, language } = state;
			let { id, icon } = type;

			let { data: playListContentBlob } = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/satmarket/${id}?language=${language}`,
				responseType: 'blob',
			});

			fileDownload(playListContentBlob, `${icon}_hmara_tv.m3u`);

			dispatch({
				type: 'USER_PLAYLIST',
				payload: { loading: false }
			});

		} catch (e) {
			dispatch({
				type: 'USER_PLAYLIST',
				payload: { loading: false }
			});
			alert(e.response ? e.response.data : e.message);
		}
	}
};

export default getPlaylist;