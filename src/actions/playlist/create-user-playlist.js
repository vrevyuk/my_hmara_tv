import axios from 'axios';

const createUserPlayList = ({ listType, title }) => {
	return async (state, dispatch) => {

		dispatch({ type: 'USER_PLAYLIST', payload: { loading: true } });

		try {
			let { accessToken, language, playLists, limit, offset } = state;

			let { data: playList } = await axios({
				method: 'post',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/site/playLists?language=${language}`,
				data: { listType, title },
			});

			let { data: playListContent } = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/site/playLists/${playList.id}`,
				params: { limit, offset, language },
			});

			dispatch({
				type: 'USER_PLAYLIST',
				payload: {
					loading: false,
					playLists: [ ...playLists, playList ],
					playListIndex: playLists.length,
					playListContent,
				}
			});

		} catch (e) {
			alert(e.message);
			dispatch({ type: 'USER_PLAYLIST', payload: { loading: false } });
		}
	}
};

export default createUserPlayList;