/**
 * Created by Vitaly Revyuk on 6/6/19.
 */
import cookie from 'react-cookies';

const logOut = () => {
	return async (state, dispatch) => {

		localStorage.removeItem('refreshToken', );
		let theSecondOrderDomain = window.location.hostname.split('.').slice(-2).join('.');
		cookie.remove('refreshToken', {
			domain: `.${theSecondOrderDomain}`,
			path: '/'
		});

		dispatch({
			type: 'AUTH_CODE',
			payload: {
				accessToken: false,
				userId: 0,
				balance: 0,
				tryingRestoreAuth: false,
				haveAuthCode: false,
				userMessages: [],
				transactions: [],
				packets: [],
				contacts: [],
				payment: null,
				drawerOpened: false,
			}
		});

	}
};

export default logOut;