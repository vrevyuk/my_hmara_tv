/**
 * Created by Vitaly Revyuk on 9/11/19.
 */

import axios from 'axios';
import i18 from '../dictionary';

const removeUserAutoPayments = () => {
    return async (state, dispatch) => {

        dispatch({type: 'I_AM_BUSY'});

        try {
            let {accessToken, recommendedSum, acquiringList} = state;
			console.log(recommendedSum, acquiringList);
            await axios({
                method: 'delete',
                headers: {
                    'Authorization': `Bearer ${accessToken}`
                },
                url: `/api/acquiring/autopayment`,
            });

			dispatch({
				type: 'USER_ACQUIRING',
				payload: {
					recommendedSum,
					acquiringList: acquiringList.map( acq => ({...acq, autoPayment: 0}))
				}
			});

		} catch (e) {
            dispatch({type: 'I_AM_NOT_BUSY'});
            let message = e.response ? e.response.data : e.message;
            alert(i18[message] ? (i18[message][state.language] || message) : message);
        }
    }
};

export default removeUserAutoPayments;