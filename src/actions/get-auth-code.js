/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import axios from 'axios';
import i18 from "../dictionary";
import ReactGA from 'react-ga';
import {isBrowser, isMobile, osName, } from 'react-device-detect';

const getAuthCode = ({contact}) => {
	return async (state, dispatch) => {

		dispatch({ type: 'I_AM_BUSY' });

		try {
			let { accessToken, getCodeTimeoutId } = state;

			let deviceId = localStorage.getItem('deviceId', );
			let language = localStorage.getItem('language', ) || 'uk';

			const {data} = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/sign?contact=${contact}&deviceId=${deviceId}&language=${language}&profileId=1&deviceType=${isMobile?20:2}&ver=${process.env.REACT_APP_VERSION}`,
			});
			dispatch({
				type: 'AUTH_CODE',
				payload: {
					codeData: data,
				}
			});

		} catch (e) {
			dispatch({ type: 'I_AM_NOT_BUSY' });
			ReactGA.event({
				category: 'account',
				action: 'login',
				label: 'wrong_contact'
			});

			let message = e.response ? e.response.data : e.message;
			alert(i18[message] ? (i18[message][state.language] || message) : message);
		}
	}
};

export default getAuthCode;