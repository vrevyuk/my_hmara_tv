/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import axios from 'axios';
import i18 from "../dictionary";

const addUserContact = ({ contact, code }) => {
	return async (state, dispatch) => {

		let { accessToken, language, contacts } = state;

		dispatch({ type: 'I_AM_BUSY' });

		try {

			await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/connect`,
				params: { contact, code, language }
			});

			if (contact && !code) {
				dispatch({
					type: 'AUTH_CODE',
					payload: {}
				});
			} else if (contact && code) {
				dispatch({
					type: 'USER_CONTACTS',
					payload: [ ...contacts, contact]
				});
			}

		} catch (e) {
			dispatch({ type: 'I_AM_NOT_BUSY' });

			let message = e.response ? e.response.data : e.message;
			alert(i18[message] ? (i18[message][language] || message) : message);
		}
	}
};

export default addUserContact;