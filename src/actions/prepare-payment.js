/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import axios from 'axios';
import i18 from '../dictionary';

const preparePayment = (props = {}) => {
	return async (state, dispatch) => {

		dispatch({ type: 'I_AM_BUSY' });

		try {
			let { acquiringUri, customer, amount, currency, autoPayment } = props;
			let { device: { protocol } } = state;

			acquiringUri = acquiringUri
				.replace('http://', `${protocol}://`)
				.replace('https://', `${protocol}://`);

			// console.log({ acquiringUri, customer, amount, currency, autoPayment });

			let { data: { action, inputs } } = await axios({
				method: 'get',
				headers: {
					'Accept': `application/json`
				},
				url: `${acquiringUri}/purchase`,
				params: { customer, amount, currency, autoPayment: autoPayment ? 1 : 0 },
			});

			dispatch({
				type: 'PREPARE_PAYMENT',
				payload: { action, inputs }
			});

		} catch (e) {
			dispatch({ type: 'I_AM_NOT_BUSY' });

			let message = e.response ? e.response.data : e.message;
			alert(i18[message] ? (i18[message][state.language] || message) : message);
		}
	}
};

export default preparePayment;