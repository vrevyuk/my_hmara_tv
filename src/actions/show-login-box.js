/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

const showLoginBox = (show = false) => {
	return async (state, dispatch) => {
		dispatch({ type: 'SHOW_LOGIN_BOX', payload: show });
	}
};

export default showLoginBox;