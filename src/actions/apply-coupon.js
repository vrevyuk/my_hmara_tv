/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import applyCouponRequest from './shared/apply-coupon-request';
import getConfig from './shared/get-config';
import i18 from "../dictionary";

const applyCoupon = () => {
	return async (state, dispatch) => {

		let { accessToken, coupon, userId, deviceId, language } = state;

		try {

			dispatch({ type: 'I_AM_BUSY' });

			await applyCouponRequest({ accessToken, language, ...coupon });

			await getConfig(dispatch, { accessToken, userId, deviceId, coupon });

			dispatch({
				type: 'SET',
				payload: { applyCouponSuccess: true, applyCouponError: null, loading: false }
			});

		} catch (e) {
			let message = e.response ? e.response.data : e.message;
			dispatch({
				type: 'SET',
				payload: {
					applyCouponSuccess: false,
					applyCouponError: i18[message] ? (i18[message][language] || message) : message,
					coupon: null,
					loading: false,
				}
			});
		}

	}
};

export default applyCoupon;