import axios from "axios";
import i18 from "../dictionary";

/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

const setLanguage = (language) => {
	return async (state, dispatch) => {

		localStorage.setItem('language', language);

		dispatch({
			type: 'SET_LANGUAGE',
			payload: language
		});
	}
};


const setRemoteLanguage = language => {
	return async (state, dispatch) => {

		localStorage.setItem('language', language);

		dispatch({
			type: 'SET_LANGUAGE',
			payload: language
		});

		let { accessToken } = state;
		try {
			let response = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/language?language=${language}`,
			});

			console.log(response);
		} catch (e) {}
	}
};

export default setRemoteLanguage;