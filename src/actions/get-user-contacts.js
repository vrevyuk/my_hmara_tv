/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import axios from 'axios';
import i18 from '../dictionary';

const getUserContacts = () => {
	return async (state, dispatch) => {

		dispatch({ type: 'I_AM_BUSY' });

		try {
			let { accessToken, language } = state;

			let { data: { contacts } } = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/s/config?fields=contacts&language=${language}`,
			});

			dispatch({
				type: 'USER_CONTACTS',
				payload: contacts
			});

		} catch (e) {
			dispatch({ type: 'I_AM_NOT_BUSY' });

			let message = e.response ? e.response.data : e.message;
			alert(i18[message] ? (i18[message][state.language] || message) : message);
		}
	}
};

export default getUserContacts;