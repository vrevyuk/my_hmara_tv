/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import axios from 'axios';
import i18 from "../dictionary";

const getAcquiringList = () => {
	return async (state, dispatch) => {

		dispatch({ type: 'I_AM_BUSY' });

		try {
			let { accessToken, language } = state;

			let { data: { recommendedSum, list: acquiringList } } = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/acquiring?language=${language}`,
			});

			dispatch({
				type: 'USER_ACQUIRING',
				payload: { recommendedSum: recommendedSum || 1, acquiringList }
			});

		} catch (e) {
			dispatch({ type: 'I_AM_NOT_BUSY' });

			let message = e.response ? e.response.data : e.message;
			alert(i18[message] ? (i18[message][state.language] || message) : message);
		}
	}
};

export default getAcquiringList;