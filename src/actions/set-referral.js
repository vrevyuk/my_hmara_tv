/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

const setReferral = (referral) => {
	return async (state, dispatch) => {
		dispatch({
			type: 'SET_REFERRAL',
			payload: referral
		});
	}
};

export default setReferral;