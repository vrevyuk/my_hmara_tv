/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import setValues from './set-values';
import cancelAction from './cancel-action';
import getAuthCode from './get-auth-code';
import checkAuthCode from './check-auth-code';
import restoreAuth from './restore-auth';
import logOut from './logout';
import getUserMessages from './get-user-messages';
import setUserMessageAsRead from './set-user-message-as-read';
import getUserTransactions from './get-user-transactions';
import getUserPackets from './get-user-packets';
import getPacketDetail from './get-packet-detail';
import setLanguage from './set-language';
import setReferral from './set-referral';
import subscribePacket from './user-subscribe-packet';
import unSubscribePacket from './user-unsubscribe-packet';
import getUserContacts from './get-user-contacts';
import addUserContact from './add-user-contact';
import openAddContactDialog from './open-add-contact-dialog';
import closeAddContactDialog from './close-add-contact-dialog';
import getAcquiringList from './get-acquiring-list';
import preparePayment from './prepare-payment';
import toggleDrawer from './toggle-drawer';
import getUserCams from './camera/get-user-cams';
import addUserCam from './camera/add-user-cam';
import updateUserCam from './camera/update-user-cam';
import deleteUserCam from './camera/delete-user-cam';
import getCouponData from './get-coupon-data';
import applyCoupon from './apply-coupon';
import showLoginBox from './show-login-box';
import getContentTypesPlaylist from './playlist/get-content_types-playlist';
import getPlaylist from './playlist/get-playlist';
import resetAllPlayLists from './playlist/reset-all-playlists';
import getActiveDevices from './playlist/get-active-devices';
import deleteActiveDevice from './playlist/delete-active-device';
import removeUserAutoPayments from './remove-auto-payments';


export {
	setValues, cancelAction,
	getAuthCode,
	checkAuthCode,
	restoreAuth,
	logOut,
	getUserMessages,
	setUserMessageAsRead,
	getUserTransactions,
	getUserPackets,
	getPacketDetail,
	setLanguage, setReferral,
	subscribePacket,
	unSubscribePacket,
	getUserContacts,
	addUserContact,
	openAddContactDialog,
	closeAddContactDialog,
	getAcquiringList,
	preparePayment,
	toggleDrawer,
	getUserCams, addUserCam, updateUserCam, deleteUserCam,
	getCouponData, applyCoupon, showLoginBox,
	getContentTypesPlaylist, getPlaylist, resetAllPlayLists, getActiveDevices, deleteActiveDevice,
	removeUserAutoPayments,
}