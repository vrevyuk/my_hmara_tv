/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

const setValues = (data) => {
	return async (state, dispatch) => {
		dispatch({ type: 'SET', payload: data });
	}
};

export default setValues;