/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import axios from 'axios';
import i18 from '../dictionary';

const getUserTransactions = () => {
	return async (state, dispatch) => {

		dispatch({ type: 'I_AM_BUSY' });

		try {
			let { accessToken, language } = state;

			let { data: { transactions } } = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/s/config?fields=transactions&language=${language}`,
			});

			dispatch({
				type: 'USER_TRANSACTIONS',
				payload: transactions
			});

		} catch (e) {
			dispatch({ type: 'I_AM_NOT_BUSY' });

			let message = e.response ? e.response.data : e.message;
			alert(i18[message] ? (i18[message][state.language] || message) : message);
		}
	}
};

export default getUserTransactions;