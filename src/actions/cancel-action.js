/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

const cancelAction = () => {
	return async (state, dispatch) => {
		dispatch({ type: 'CANCEL_ACTION' });
	}
};

export default cancelAction;