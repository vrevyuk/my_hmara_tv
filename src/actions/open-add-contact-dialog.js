/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

const openAddContactDialog = () => {
	return async (state, dispatch) => {
		dispatch({ type: 'OPEN_ADD_CONTACT_DIALOG' });
	}
};

export default openAddContactDialog;