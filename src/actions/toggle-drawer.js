/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

const toggleDrawer = () => {
	return async (state, dispatch) => {
		dispatch({ type: 'TOGGLE_DRAWER' });
	}
};

export default toggleDrawer;