/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import axios from 'axios';
import uuid4 from 'uuid/v4';
import getConfig from './shared/get-config';
import i18 from '../dictionary';
import cookie from 'react-cookies';
import { isBrowser, isMobile } from 'react-device-detect';

const restoreAuth = () => {
	return async (state, dispatch) => {

		let oldRefreshToken = cookie.load('refreshToken') || localStorage.getItem('refreshToken', );
		let deviceId = localStorage.getItem('deviceId', );
		let language = localStorage.getItem('language');

		if (!deviceId) {
			deviceId = uuid4();
			localStorage.setItem('deviceId', deviceId);
		}

		if (oldRefreshToken) {

			dispatch({ type: 'TRYING_RESTORE_AUTH', payload: { language } });

			try {

				let { data } = await axios({
					method: 'get',
					headers: {
						'Authorization': `Bearer ${state.accessToken}`
					},
					url: `/api/sign?refreshToken=${oldRefreshToken}&deviceId=${deviceId}&language=${language}&profileId=1&deviceType=${isMobile?20:2}&ver=${process.env.REACT_APP_VERSION}`
				});

				let {accessToken, refreshToken, portal} = data;

				await getConfig(dispatch, { accessToken, refreshToken, portal, deviceId, language });

			} catch (e) {
				dispatch({
					type: 'AUTH_CODE',
					payload: {language}
				});
			}
		}
	}
};

export default restoreAuth;