/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

const closeAddContactDialog = () => {
	return async (state, dispatch) => {
		dispatch({ type: 'CLOSE_ADD_CONTACT_DIALOG' });
	}
};

export default closeAddContactDialog;