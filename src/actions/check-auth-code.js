/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import axios from 'axios';
import getConfig from './shared/get-config';
import applyCouponRequest from './shared/apply-coupon-request';
import i18 from "../dictionary";
import ReactGA from 'react-ga';
import { isBrowser, isMobile } from 'react-device-detect';

const checkAuthCode = ({ contact, code, referral }) => {
	return async (state, dispatch) => {

		dispatch({ type: 'I_AM_BUSY' });

		try {

			let deviceId = localStorage.getItem('deviceId', );
			let language = localStorage.getItem('language', ) || 'uk';

			let { data } = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${state.accessToken}`
				},
				url: `/api/sign`,
				params: { contact, code, referral, deviceId, language, profileId: 1, deviceType: isMobile?20:2, ver: process.env.REACT_APP_VERSION}
			});

			let {accessToken, refreshToken, portal} = data;
			let { coupon } = state;

			let applyCouponSuccess, applyCouponError;
			if (coupon) {
				try {
					await applyCouponRequest({ accessToken, ...coupon });
					applyCouponSuccess = true;
					applyCouponError = null;
				} catch (e) {
					let message = e.response ? e.response.data : e.message;
					applyCouponSuccess = false;
					applyCouponError = i18[message] ? (i18[message][state.language] || message) : message;
				}
			}

			await getConfig(dispatch, {accessToken, refreshToken, portal, deviceId, applyCouponSuccess, applyCouponError, language});

			ReactGA.event({
				category: 'account',
				action: 'login',
				label: 'success'
			});

		} catch (e) {
			dispatch({ type: 'I_AM_NOT_BUSY' });
			ReactGA.event({
				category: 'account',
				action: 'login',
				label: 'failure'
			});

			let message = e.response ? e.response.data : e.message;
			alert(i18[message] ? (i18[message][state.language] || message) : message);
		}
	}
};

export default checkAuthCode;