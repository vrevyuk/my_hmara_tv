/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import axios from 'axios';

const updateUserCam = (cameraId, cameraData) => {
	return async (state, dispatch) => {

		dispatch({ type: 'I_AM_BUSY' });

		try {
			let { accessToken, language } = state;
			console.log({ cameraId, cameraData });

			let { data: camera } = await axios({
				method: 'post',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/site/cameras/${cameraId}`,
				data: { language, ...cameraData }
			});

			dispatch({
				type: 'USER_CAMERA_UPDATE',
				payload: camera
			});

		} catch (e) {
			alert(e.message);
			dispatch({ type: 'I_AM_NOT_BUSY' });
		}
	}
};

export default updateUserCam;