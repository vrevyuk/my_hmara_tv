/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import axios from 'axios';

const addUserCam = (cameraData) => {
	return async (state, dispatch) => {

		dispatch({ type: 'I_AM_BUSY' });

		try {
			let { accessToken, language } = state;

			let { data: cameras } = await axios({
				method: 'post',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/site/cameras`,
				data: { language, ...cameraData }
			});

			dispatch({
				type: 'USER_CAMERAS',
				payload: cameras
			});

		} catch (e) {
			alert(e.message);
			dispatch({ type: 'I_AM_NOT_BUSY' });
		}
	}
};

export default addUserCam;