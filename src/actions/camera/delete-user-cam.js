/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import axios from 'axios';

const deleteUserCam = ({ cameraId }) => {
	return async (state, dispatch) => {

		dispatch({ type: 'I_AM_BUSY' });

		try {
			let { accessToken } = state;

			await axios({
				method: 'delete',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/site/cameras/${cameraId}`,
			});

			dispatch({
				type: 'USER_CAMERA_DELETE',
				payload: cameraId
			});

		} catch (e) {
			alert(e.message);
			dispatch({ type: 'I_AM_NOT_BUSY' });
		}
	}
};

export default deleteUserCam;