/**
 * Created by Vitaly Revyuk on 6/10/19.
 */
import axios from 'axios';

const applyCouponRequest = async ({ coupon, accessToken }) => {

	return axios({
		method: 'get',
		headers: {
			'Authorization': `Bearer ${accessToken}`
		},
		url: `/api/promo/${coupon}`,
	});

};

export default applyCouponRequest;