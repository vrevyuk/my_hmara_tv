/**
 * Created by Vitaly Revyuk on 6/10/19.
 */
import axios from 'axios';
import cookie from 'react-cookies';

async function getConfig(dispatch, userProps = {}) {

	let { accessToken, refreshToken, portal, deviceId, applyCouponSuccess, applyCouponError, language } = userProps;


	// refreshToken && localStorage.setItem('refreshToken', refreshToken);
	let theSecondOrderDomain = window.location.hostname.split('.').slice(-2).join('.');
	refreshToken && cookie.save('refreshToken', refreshToken, {
		domain: `.${theSecondOrderDomain}`,
		path: '/'
	});

	let { data: {userId, asGuest, contacts, currency, balance, setting, referralUrl, playlist } } = await axios({
		method: 'get',
		headers: {
			'Authorization': `Bearer ${accessToken}`
		},
		url: `/api/s/config?fields=contacts,currency,balance,setting,referralUrl,playlist`,
	});

	if (parseInt(asGuest, 10) === 1) return dispatch({
		type: 'AUTH_CODE',
		payload: {}
	});

	dispatch({
		type: 'AUTH_CODE',
		payload: {
			accessToken,
			refreshToken,
			portal,
			userId,
			language,
			deviceId,
			contacts,
			currency,
			balance,
			setting,
			referralUrl,
			playlist,
			haveAuthCode: false,
			referral: undefined,
			isLoginBoxShows: false,
			applyCouponSuccess, applyCouponError,
		}
	});
}


export default getConfig;