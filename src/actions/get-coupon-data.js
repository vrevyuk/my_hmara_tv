/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import axios from 'axios';
import uuid4 from 'uuid/v4';
import i18 from '../dictionary';
import { isBrowser, isMobile } from 'react-device-detect';

const getCouponData = (coupon) => {
	return async (state, dispatch) => {

		if (!coupon) return;

		try {
			const language = localStorage.getItem('language');
			let deviceId = localStorage.getItem('deviceId');

			if (!deviceId) {
				deviceId = uuid4();
				localStorage.setItem('deviceId', deviceId);
			}

			dispatch({
				type: 'SET',
				payload: { coupon: null, loading: true }
			});

			const { data: { accessToken } = {} } = await axios({
				method: 'get',
				url: `/api/sign?deviceId=${deviceId}&language=${language}&profileId=1&deviceType=${isMobile?20:2}&ver=${process.env.REACT_APP_VERSION}`,
			});

			const { data = {} } = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/promo/preview/${coupon}?language=${language}`,
			});

			dispatch({
				type: 'SET',
				payload: {coupon: data, loading: false, language}
			});

		} catch (e) {
			dispatch({
				type: 'SET',
				payload: { coupon: null, loading: false }
			});

			let message = e.response ? e.response.data : e.message;
			alert(i18[message] ? (i18[message][state.language] || message) : message);
		}
	}
};

export default getCouponData;