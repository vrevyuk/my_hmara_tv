/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

import axios from 'axios';
import i18 from '../dictionary';

const getPacketDetail = (packetId) => {
	return async (state, dispatch) => {

		dispatch({
			type: 'PACKET_DETAIL',
			payload: {
				packetDetail: [],
				loading: true
			}
		});

		try {
			let { accessToken, language } = state;

			let { data } = await axios({
				method: 'get',
				headers: {
					'Authorization': `Bearer ${accessToken}`
				},
				url: `/api/site/packets/${packetId}?type=1&language=${language}`,
			});

			dispatch({
				type: 'PACKET_DETAIL',
				payload: {
					packetDetail: data,
					loading: false
				}
			});

		} catch (e) {
			dispatch({ type: 'I_AM_NOT_BUSY' });

			let message = e.response ? e.response.data : e.message;
			alert(i18[message] ? (i18[message][state.language] || message) : message);
		}
	}
};

export default getPacketDetail;