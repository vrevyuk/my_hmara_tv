
import HomeSVG from '../assets/home';
import SubscriptionsSVG from '../assets/subscriptions';
import PaymentSVG from '../assets/payment';
import TransactionsSVG from '../assets/transactions';
import MessagesSVG from '../assets/messages';
import CameraSVG from '../assets/camera';
import ContactsSVG from '../assets/contacts';
import PromoSVG from '../assets/promo';
import ExitSVG from '../assets/exit';
import TileBalanceSVG from './tile-balance';
import TileCameraSVG from './tile-camera';
import TileContactsSVG from './tile-contacts';
import TileMessagesSVG from './tile-messages';
import TilePaymentSVG from './tile-payment';
import TilePortalSVG from './tile-portal';
import TilePromoSVG from './tile-promo';
import TileSubscriptionsSVG from './tile-subscriptions';

export {
    HomeSVG, SubscriptionsSVG, PaymentSVG, TransactionsSVG, MessagesSVG, CameraSVG, ContactsSVG, PromoSVG, ExitSVG,
    TileBalanceSVG, TileCameraSVG, TileContactsSVG, TileMessagesSVG, TilePaymentSVG, TilePortalSVG,
    TilePromoSVG, TileSubscriptionsSVG,
}