import React from 'react';
import ReactDOM from 'react-dom';
import { AppProvider } from './context';
import App from './app';
import * as serviceWorker from './serviceWorker';
import ReactGA from 'react-ga';

if (process.env.NODE_ENV === 'development') {
	window['ga-disable-UA-131143143-1'] = true;
	console.warn(`Google analytics is disabled.`);
}

ReactGA.initialize('UA-131143143-1');

ReactDOM.render(
	<AppProvider>
		<App/>
	</AppProvider>,
	document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
