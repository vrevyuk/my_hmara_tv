/**
 * Created by Vitaly Revyuk on 6/5/19.
 */

const reducer = (state = {}, action) => {
	let { type, payload } = action;
	// process.env.NODE_ENV === "development" && console.log({state, type, payload});

	switch (type) {
		case 'SET':
			return { ...state, ...payload };

		case 'I_AM_BUSY':
			return { ...state, loading: true };
		case 'I_AM_NOT_BUSY':
			return { ...state, loading: false };
		case 'CANCEL_ACTION':
			return { ...state, loading: false, haveAuthCode: false, tryingRestoreAuth: false };
		case 'SHOW_LOGIN_BOX':
			return { ...state, isLoginBoxShows: payload };

		case 'TRYING_RESTORE_AUTH':
			return { ...state, tryingRestoreAuth: true, language: payload.language };
		case 'AUTH_CODE':
			return { ...state, loading: false, haveAuthCode: true, tryingRestoreAuth: false, ...payload };

		case 'ACTION_TEXT':
			return { ...state, loading: false, text: payload };

		case 'USER_MESSAGES':
			return { ...state, loading: false, userMessages: payload };
		case 'USER_MESSAGE_SET_AS_READ': return { ...state, loading: false, userMessages: state.userMessages.map( mess => mess.id === payload ? { ...mess, isRead: 1 } : mess ) };

		case 'USER_TRANSACTIONS':
			return { ...state, loading: false, transactions: payload };
		case 'USER_PACKETS':
			return { ...state, loading: false, packets: payload };
		case 'PACKET_DETAIL':
			return { ...state, loading: payload.loading, packetDetail: payload.packetDetail };
		case 'USER_CONTACTS':
			return { ...state, loading: false, haveAuthCode: false, addContactDialog: false, contacts: payload };

		case 'SET_LANGUAGE':
			return { ...state, loading: false, language: payload, drawerOpened: false };
		case 'SET_REFERRAL':
			return { ...state, loading: false, referral: payload };
		case 'SET_COUPON':
			return { ...state, loading: false, ...payload };
		case 'APPLY_COUPON_DONE':
			return { ...state, loading: false, applyCouponSuccess: true };

		case 'OPEN_ADD_CONTACT_DIALOG':
			return { ...state, addContactDialog: true, haveAuthCode: false };
		case 'CLOSE_ADD_CONTACT_DIALOG':
			return { ...state, addContactDialog: false, haveAuthCode: false };
		case 'USER_ACQUIRING': {
			let { recommendedSum = 1, acquiringList = [] } = payload;
			return { ...state, loading: false, recommendedSum, acquiringList };
		}
		case 'PREPARE_PAYMENT':
			return { ...state, loading: false, payment: payload };
		case 'TOGGLE_DRAWER':
			return { ...state, loading: false, drawerOpened: !state.drawerOpened };

		case 'USER_CAMERAS':
			return { ...state, loading: false, cameras: payload };
		case 'USER_CAMERA_DELETE':
			return {
				...state,
				loading: false,
				cameras: state.cameras.filter( cam => cam.cameraId !== payload )
			};
		case 'USER_CAMERA_UPDATE':
			return {
				...state,
				loading: false,
				cameras: state.cameras.map( cam => cam.cameraId === payload.cameraId ? payload : cam )
			};

		case 'USER_PLAYLIST': {
			let {loading, contentTypes, activeDevices, playlist} = payload;
			let newState = {...state, loading};
			newState = contentTypes ? {...newState, contentTypes} : newState;
			newState = activeDevices ? {...newState, activeDevices} : newState;
			newState = playlist ? {...newState, playlist} : newState;
			return newState;
		}

		default:
			return { ...state }
	}
};

export default reducer;