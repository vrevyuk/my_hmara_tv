/**
 * Created by Vitalii Reviuk on 6/7/19.
 */

export default portal => {
	let portalWord;
	portal = portal || window.location.hostname;

	switch (portal) {
		case 'localhost': portalWord = {
			uk: 'localhost ТБ',
			en: 'localhost TV',
			ru: 'localhost ТВ'
		}; break;

		case 'my.vivat.live': portalWord = {
			uk: 'Виват ТВ',
			en: 'Виват ТВ',
			ru: 'Виват ТВ'
		}; break;

		case 'my.hmara.tv':
		default: portalWord = {
			uk: 'Хмара ТБ',
			en: 'Hmara TV',
			ru: 'Хмара ТВ'
		}
	}

	return {
		appTitle: {
			uk: '{portal_name}'.replace(/{portal_name}/ig, portalWord.uk),
			en: '{portal_name}'.replace(/{portal_name}/ig, portalWord.en),
			ru: '{portal_name}'.replace(/{portal_name}/ig, portalWord.ru)
		},
		menuReferralDescriptionFull: {
			uk: 'Кожен абонент {portal_name} має можливість взяти участь в реферальній програмі. Ви будете отримувати 10% від кожного платежу всіх запрошених вами користувачів (рефералів). Підключений користувач отримає знижку в 5% на любий тарифний пакет. Для цього скопіюйте вказане нижче посилання та передайте його любій кількості людей. Після регістрації за допомою посилання, запрошений стає вашим рефералом і ви сплачувати послуги {portal_name} з получених бонусів. Посилання для розповсюдження: '.replace(/{portal_name}/ig, portalWord.uk),
			en: 'Кожен абонент {portal_name} має можливість взяти участь в реферальній програмі. Ви будете отримувати 10% від кожного платежу всіх запрошених вами користувачів (рефералів). Підключений користувач отримає знижку в 5% на любий тарифний пакет. Для цього скопіюйте вказане нижче посилання та передайте його любій кількості людей. Після регістрації за допомою посилання, запрошений стає вашим рефералом і ви сплачувати послуги {portal_name} з получених бонусів. Посилання для розповсюдження: '.replace(/{portal_name}/ig, portalWord.en),
			ru: 'Каждый абонент {portal_name} имеет возможность поучаствовать в реферальной программе. Вы будете получать 10% от каждого платежа всех приглашённых Вами пользователей (рефералов). Подключенный пользователь получает скидку 5% на любой тарифный пакет. Для этого скопируйте указанную ссылку и передайте её любому количеству людей. После регистрации по ссылке, приглашённый станет Вашим рефералом и Вы сможете оплачивать услуги {portal_name} с полученных бонусов. Ссылка для распространения: '.replace(/{portal_name}/ig, portalWord.ru)
		},
		referralDialogText: {
			uk: 'Ви отримали запрошення на портал мультимедійних розваг {portal_name}. Після регістрації ви отримаєте знижку на всі тарифи для перегляду онлайн ТБ, а також безоплатний тестовий доступ до преміум пакету. Безоплатний тестовий доступ надається тільки при регістрації по номеру телефону.'.replace(/{portal_name}/ig, portalWord.uk),
			en: 'You have received an invitation to an entertainment portal {portal_name}. After registration, you will receive a discount on all packages for watching online TV, as well as free trial access to premium packages. Free access will be provided if you register only by phone number.'.replace(/{portal_name}/ig, portalWord.en),
			ru: 'Вы получили приглашение на портал мультимедийных развлечений {portal_name}. После регистрации Вы получите скидку на все тарифы для просмотра онлайн ТВ, а также бесплатный тестовый доступ к премиумному пакету. Бесплатный тестовый доступ предоставляется только при регистрации по номеру телефона.'.replace(/{portal_name}/ig, portalWord.ru)
		},
		promoDialogText: {
			uk: 'Введіть ваш промокод та натисніть кнопку "Перевірити" для отримання інформації о наданих акціях.'.replace(/{portal_name}/ig, portalWord.uk),
			en: 'Type your promo-code and press button "Check out" for getting information about all available stocks.'.replace(/{portal_name}/ig, portalWord.en),
			ru: 'Введите ваш промокод и нажмите кнопку "Проверить" для получения информации о предоставленных акциях.\n'.replace(/{portal_name}/ig, portalWord.ru)
		},
		promoDialogText_OLD: {
			uk: 'Ви отримали запрошення на портал мультимедійних розваг {portal_name}. Після регістрації ви отримаєте знижку на всі тарифи для перегляду онлайн ТБ, а також безоплатний тестовий доступ до преміум пакету. Безоплатний тестовий доступ надається тільки при регістрації по номеру телефону.'.replace(/{portal_name}/ig, portalWord.uk),
			en: 'You have received an invitation to an entertainment portal {portal_name}. After registration, you will receive a discount on all packages for watching online TV, as well as free trial access to premium packages. Free access will be provided if you register only by phone number.'.replace(/{portal_name}/ig, portalWord.en),
			ru: 'Вы получили приглашение на портал мультимедийных развлечений {portal_name}. После регистрации Вы получите скидку на все тарифы для просмотра онлайн ТВ, а также бесплатный тестовый доступ к премиумному пакету. Бесплатный тестовый доступ предоставляется только при регистрации по номеру телефона.'.replace(/{portal_name}/ig, portalWord.ru)
		},
		newCameraText: {
			uk: `Шановний абонент.<br />У цьому розділі ви можете самостійно ввести посилання на особисту ip-камеру.
			Наприклад, камеру охорони периметра будинку або роботи. Всі введені посилання строго конфіденційні та доступні до перегляду тільки вам.
			Посилання на "реальних" ip-адресах будуть доступні до перегляду поза домом / роботи, посилання на "сірих" ip-адресах (формат 10. *. *. * І 192.168. *. *) Будуть доступні тільки вдома (за роутером).
			<br /><br />
			Вбудований відеоплеєр вашого ТВ відтворює посилання з розширенням .mp4 та .m3u8
			Якщо у вас поток з камери іншого формату, ми можемо провести його переформатування в потрібний формат. Послуга переформатування до 3 потоків безкоштовна.
			Для замовлення напишіть нам на пошту webcam@hmara.tv`.replace(/{portal_name}/ig, portalWord.uk),
			en: `Dear subscriber.<br />In this section you can independently enter the link to your personal ip-camera.
			For example, the perimeter security camera at home or work. All entered links are strictly confidential and available for viewing only to you.
			Links to the "real" ip-addresses will be available for viewing outside the home / work, links to the "gray" ip-addresses (format 10. *. *. * And 192.168. *. *) will be available only at home.
			<br /><br />
			The built-in video player of your TV plays links with the extension .mp4 and .m3u8
			If you have a stream from a camera of a different format, we can recode it into the desired format. Recoding service up to 3 streams is free.
			To order email us at webcam@hmara.tv`.replace(/{portal_name}/ig, portalWord.uk),
			ru: `Уважаемый абонент.<br /> В этом разделе вы можете самостоятельно ввести ссылку на личную ip-камеру. 
			Например, камеру охраны периметра дома или работы. Все введенные ссылки строго конфиденциальны и доступны к просмотру только вам. 
			Ссылки на "реальных" ip-адресах будут доступны к просмотру вне дома/работы, ссылки на "серых" ip-адресах (формат 10.*.*.* и 192.168.*.*) будут доступны только дома (за роутером).
			<br /><br />
			Встроенный видеоплеер вашего ТВ воспроизводит ссылки с расширением .mp4 и .m3u8
			Если у вас поток с камеры другого формата, мы можем провести ее переформатирование в нужный формат. Услуга  переформатирования  до 3 потоков бесплатна. 
			Для заказа напишите нам на почту webcam@hmara.tv`.replace(/{portal_name}/ig, portalWord.ru),
		},

	}
}
