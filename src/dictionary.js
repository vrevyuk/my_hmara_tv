/**
 * Created by Vitalii Reviuk on 6/7/19.
 */

export default {
	cannot_delete_grouped_packet: {
		uk: 'Для відключення цього пакету потрібно підключити другий з такої ж групи',
		en: 'To unsubscribe from this packet, you must subscribe another packet with the same group',
		ru: 'Для отключения этого пакета необходимо подключить другой пакет с такой же группы'
	},
	not_available_in_our_country: {
		uk: 'Не доступно в цьому регіоні',
		en: 'Not available in our country',
		ru: 'Не доступно в этом регионе'
	},
	coupon_not_available: {
		uk: 'Промо код не валідний або вже вами використаний',
		en: 'Coupon is not valid or you have used it',
		ru: 'Промо код не действительный или уже ранее вами использованный'
	},
	contact_already_used: {
		uk: 'Даний контакт вже використовується',
		en: 'This contact is already in use',
		ru: 'Данный контакт уже используется'
	},
	wrong_number: {
		uk: 'Номер телефону не є дійсним',
		en: 'The phone number is not exists',
		ru: 'Номер телефона не действителен'
	},
	user_or_code_not_found: {
		uk: 'Номер телефону не є дійсним або невірно вказаний код',
		en: 'The phone number is not exists or code is wrong',
		ru: 'Номер телефона не действителен или неверно указан код'
	},
	loading: {
		uk: 'Будь ласка почекайте, йде отримання данних ...',
		en: 'Please wait, retrieving data from server ...',
		ru: 'Пожалуйста подождите, идет получение данных ...'
	},
	login: {
		uk: 'Вхід',
		en: 'Login',
		ru: 'Вход'
	},
	languageUK: {
		uk: 'Українська',
		en: 'Ukrainian',
		ru: 'Украинский'
	},
	languageEN: {
		uk: 'Англійська',
		en: 'English',
		ru: 'Английский'
	},
	languageRU: {
		uk: 'Російська',
		en: 'Russian',
		ru: 'Русский'
	},
	balance: {
		uk: 'БАЛАНС',
		en: 'BALANCE',
		ru: 'БАЛАНС'
	},
	UAH: {
		uk: 'грн',
		en: 'uah',
		ru: 'грн'
	},
	PLN: {
		uk: 'злотих',
		en: 'pln',
		ru: 'злотых'
	},
	EUR: {
		uk: 'євро',
		en: 'eur',
		ru: 'евро'
	},
	USD: {
		uk: 'доларів',
		en: 'usd',
		ru: 'долларов'
	},
	RUB: {
		uk: 'рублів',
		en: 'rub',
		ru: 'рублей'
	},
	menuHome: {
		uk: 'ГОЛОВНА',
		en: 'HOME',
		ru: 'ГЛАВНАЯ'
	},
	menuPackets: {
		uk: 'ПІДПИСКИ',
		en: 'SUBSCRIPTIONS',
		ru: 'ПОДПИСКИ'
	},
	menuPacketsDescription: {
		uk: 'Інформація про вартість підписки, вибір пакетів',
		en: 'Manage packets and subscriptions',
		ru: 'Информация о стоимости подписки, выбор доп. пакетов'
	},
	menuMessages: {
		uk: 'ПОВІДОМЛЕННЯ',
		en: 'MESSAGES',
		ru: 'УВЕДОМЛЕНИЯ'
	},
	menuMessagesDescription: {
		uk: 'Перегляд повідомленнь',
		en: 'Here are messages',
		ru: 'Просмотр сообщений'
	},
	menuTransactions: {
		uk: 'ТРАНЗАКЦІЇ',
		en: 'TRANSACTIONS',
		ru: 'ТРАНЗАКЦИИ'
	},
	menuTransactionsDescription: {
		uk: 'Перегляд стану рахунка',
		en: 'Here you can see money traffic',
		ru: 'Просмотр состояния счета'
	},
	menuPlayer: {
		uk: 'ДИВИТИСЬ',
		en: 'WATCH TV',
		ru: 'СМОТРЕТЬ'
	},
	menuPlayerDescription: {
		uk: 'Онлайн перегляд',
		en: 'Watch TV online',
		ru: 'Онлайн просмотр'
	},
	menuContacts: {
		uk: 'МОЇ КОНТАКТИ',
		en: 'MY CONTACTS',
		ru: 'МОИ КОНТАКТЫ'
	},
	menuContactsDescription: {
		uk: 'Добавити або переглянути контактні дані',
		en: 'Here you can see or add your contacts',
		ru: 'Внесение и просмотр контактных данных'
	},
	menuMakePayment: {
		uk: 'ОПЛАТИТИ ПОСЛУГИ',
		en: 'MAKE PAYMENT',
		ru: 'ОПЛАТИТЬ УСЛУГИ'
	},
	menuMakePaymentDescription: {
		uk: 'Інформація про способи оплати послуг',
		en: 'This section helps to make payment',
		ru: 'Информация о способах оплаты услуги'
	},
	menuLogout: {
		uk: 'ЗАВЕРШИТИ СЕСІЮ',
		en: 'LOG OUT',
		ru: 'ЗАВЕРШИТЬ СЕССИЮ'
	},
	menuLogin: {
		uk: 'ВХІД',
		en: 'LOG IN',
		ru: 'ВХОД'
	},
	menuCameras: {
		uk: 'КАМЕРИ КОРИСТУВАЧА',
		en: 'USER\'S CAMERAS',
		ru: 'КАМЕРЫ ПОЛЬЗОВАТЕЛЯ'
	},
	menuCamerasDescription: {
		uk: 'Встановлення камер користувача',
		en: 'Manage user\'s cameras',
		ru: 'Установка личных камер абонента'
	},
	menuPromo: {
		uk: 'ПРОМО КОД',
		en: 'DISCOUNT CODE',
		ru: 'ПРОМО КОД'
	},
	menuPromoDescription: {
		uk: 'Активація акційного промокоду',
		en: 'Activation of discount code',
		ru: 'Ввод акционного промокода'
	},
	menuFeedback: {
		uk: 'Зворотній зв`язок',
		en: 'Feedback',
		ru: 'Обратная связь'
	},
	menuReferral: {
		uk: 'РЕФЕРАЛЬНА ПРОГРАМА',
		en: 'REFERRAL PROGRAMME',
		ru: 'РЕФЕРАЛЬНАЯ ПРОГРАММА'
	},
	menuReferralDescription: {
		uk: 'Заробляй з нами',
		en: 'Earn money with us',
		ru: 'Зарабатывай с нами'
	},
	menuPlayList: {
		uk: 'СПИСОК ВІДТВОРЕННЯ',
		en: 'PLAY LIST',
		ru: 'ПЛЕЙЛИСТЫ'
	},
	menuPlayListDescription: {
		uk: 'Списки відтворення контенту',
		en: 'Content\'s play lists',
		ru: 'Списки воспроизведения контента'
	},
	HOUR: {
		uk: 'за годину',
		en: 'per a hour',
		ru: 'за час'
	},
	DAY: {
		uk: 'за день',
		en: 'per a day',
		ru: 'за день'
	},
	WEEK: {
		uk: 'за тиждень',
		en: 'per a week',
		ru: 'за неделю'
	},
	MONTH: {
		uk: 'за місяць',
		en: 'per a month',
		ru: 'за месяц'
	},
	YEAR: {
		uk: 'за рік',
		en: 'per a year',
		ru: 'за год'
	},
	expired: {
		uk: 'наступний термін списання за пакет',
		en: 'expired in',
		ru: 'следующее спсиание за пакет'
	},
	transactionsDate: {
		uk: 'дата платежу',
		en: 'date of payment',
		ru: 'дата платежа'
	},
	transactionsSum: {
		uk: 'сума',
		en: 'sum',
		ru: 'сумма'
	},
	transactionsDescription: {
		uk: 'коментарі',
		en: 'description',
		ru: 'комментарии'
	},
	messagesDate: {
		uk: 'дата',
		en: 'date',
		ru: 'дата'
	},
	messagesMessage: {
		uk: 'повідомлення',
		en: 'message',
		ru: 'сообщение'
	},
	tryingRestoreAuth: {
		uk: 'Аплікація намагається відновити авторизацію',
		en: 'The app is trying to restore authorization ...',
		ru: 'Приложение переавторизируется'
	},
	loginHeader: {
		uk: 'Автентифікація',
		en: 'Authentication',
		ru: 'Аутентификация'
	},
	contactPlaceholder: {
		uk: 'Телефон чи e-mail',
		en: 'Phone or email address',
		ru: 'Телефон или e-mail'
	},
	codePlaceholder: {
		uk: 'Код з СМС',
		en: 'Code from sms or email',
		ru: 'Код из СМС'
	},
	loginText: {
		uk: 'Введіть номер вашого телефону (без плюса в форматі 380ХХХХХХХХХ) або email адресу та натисніть "Отримати код", після того як отримаєте email чи смс повідомдення з кодом введіть код та натисніть "Перевірити код".',
		en: 'You should enter your phone number (without a plus using a format 380ХХХХХХХХХ) or email then get auth code from SMS or e-mail and enter one to the field.',
		ru: 'Введите номер вашего телефона (без плюса в формате 380ХХХХХХХХХ) или email адрес и нажмите "Получить код", после того как получите email или смс сообщение с кодом введите код и нажмите "Проверить код"'
	},
	loginText2: {
		uk: 'Не використаний код дійсний протягом 24 годин з моменту стврення. Ви можете отримати 3 сповіщення на 1 номер чи email протягом доби.',
		en: 'Unused code is valid for 24 hours after creation. You can get 3 messages for 1 phone number or email during a day.',
		ru: 'Не использованный код действителен в течение 24 часов с момента создания. Вы можете получить 3 сообщения на 1 номер в течение суток.'
	},
	cancelBtn: {
		uk: 'Відміна',
		en: 'Cancel',
		ru: 'Отмена'
	},
	cancelContactBtn: {
		uk: 'Почати заново',
		en: 'New start',
		ru: 'Начать сначала'
	},
	getCodeBtn: {
		uk: 'Отримати код',
		en: 'Get',
		ru: 'Получить код'
	},
	getCodeBtnAgain: {
		uk: 'Отримати код ще раз',
		en: 'Get code again',
		ru: 'Получить код еще раз'
	},
	remainAttempts: {
		uk: 'Залишилось SMS на сьогодні: %num%',
		en: 'Remain SMS for today: %num%',
		ru: 'Осталось SMS на сегодня: %num%'
	},
	checkCodeBtn: {
		uk: 'Перевірити код',
		en: 'Verify',
		ru: 'Проверить код'
	},
	addNewContactHeader: {
		uk: 'Додати новий контакт',
		en: 'Add new contact',
		ru: 'Добавить новый контакт'
	},
	addNewContactText: {
		uk: 'Введіть номер вашого телефону або email адресу та натисніть "Отримати код", після того як отримаєте email чи смс повідомдення з кодом введіть код та натисніть "Перевірити код"',
		en: 'You should enter your phone number or email then get auth code from SMS or e-mail and enter one to the field.',
		ru: 'Введите номер вашего телефона или email адрес и нажмите "Получить код", после того как получите email или смс сообщение с кодом введите код и нажмите "Проверить код"'
	},
	subscribeBtn: {
		uk: 'Підключити',
		en: 'Subscribe',
		ru: 'Подключить'
	},
	unsubscribeBtn: {
		uk: 'Відключити',
		en: 'Unsubscribe',
		ru: 'Отключить'
	},
	confirmSubscribe: {
		uk: 'Підключити? Списання коштів в розмірі денної/місячної суми відбувається після включення тарифу або послуги.',
		en: 'Plug in? Debit / monthly amount deduction occurs after the tariff or service has been included.',
		ru: 'Подключить? Списание будет в размере дневной/месячной суммы сразу после включения тарифа или услуги.'
	},
	confirmUnSubscribe: {
		uk: 'Відключити?',
		en: 'Switch off?',
		ru: 'Отключить?'
	},
	enabledAutoPayment: {
		uk: 'Активовано автоматичне списання',
		en: 'Activated auto replenishment',
		ru: 'Активировано автоматическое списние'
	},
	paymentCompleteHeader: {
		uk: 'Платіж завершено',
		en: 'You have completed payment',
		ru: 'Платеж завершен'
	},
	paymentCompleteText: {
		uk: 'Ви завершили формування платежу. Після успішного завершення на сайті платіжної системи ви отримаєте можливість використати кошти зазвичай за 5-10 хвилин, в окремих випадках платіж може проводитись до 24 годин.',
		en: 'You have completed the payment. After successful completion on the payment system page, you can use your credit after 5-10 minutes, in a special case, payment can be completed within 24 hours.',
		ru: 'Вы завершили формирование платежа. После успешного завершения на сайте платежной системы вы получите возможность использовать средства обычно через 5-10 минут, в отдельных случаях платеж может проводитися до 24 часов.'
	},
	paymentIWantToUseAction: {
		uk: 'Я хочу брати участь в акції "10 + 2"',
		en: 'I want to use discount "10 + 2"',
		ru: 'Я хочу учавствовать в акции "10 + 2"'
	},
	paymentIDontWantToUse: {
		uk: 'Я не хочу використати акцію "10 + 2"',
		en: 'I don\'t want to use discount "10 + 2"',
		ru: 'Я не хочу учавствовать в акции "10 + 2"'
	},
	paymentTenPlusTwo: {
		uk: 'При оплаті послуги на 10 місяців ви отримаєте 2 місяці поточного тарифу безоплатно. Зверніть увагу, що сума розрахована для вашого акаунту, через котрий ви ввійшли на цей сайт. Для поповнення чужого акаунту дізнайтесь точну сумму оплати за місяць та помножте її на 10.',
		en: 'If you pay for services for 10 months, you will receive 2 months of the current tariff for free. Please note that the amount is calculated for your account, which you used for the current site. To replenish someone\'s personal account, find out the exact amount of payment per month and multiply by 10.',
		ru: 'При оплате услуги на 10 месяцев, Вы получите 2 месяца текущего тарифа бесплатно. Обратите внимание, что сумма рассчитана для Вашего аккаунта, через который Вы вошли на текущий сайт. Для пополнения чужого лицевого счёта, узнайте точную сумму оплаты за месяц и умножьте на 10.'
	},
	paymentAmount: {
		uk: 'Сума',
		en: 'Amount',
		ru: 'Сумма'
	},
	paymentAcquiringDescription: {
		uk: 'Опис платіжної системи',
		en: 'About the acquiring',
		ru: 'Описание платежной системы'
	},
	paymentPreparePayment: {
		uk: 'Підготовка платежу ...',
		en: 'Preparing payment ...',
		ru: 'Подготовка платежа ...'
	},
	paymentMake: {
		uk: 'Продовжити на сайті платіжної системи',
		en: 'Continue on the acquiring site',
		ru: 'Продолжить на сайте платежной системы'
	},
	addAutoPaymentCheckbox: {
		uk: 'Налаштувати автоматичне списання по закінченню балансу?',
		en: 'Allow to automatic account replenishment?',
		ru: 'Настроить автоматическое списание по окончанию баланса?'
	},
	paymentNextBtn: {
		uk: 'Далі',
		en: 'Next',
		ru: 'Дальше'
	},
	paymentBackBtn: {
		uk: 'Назад',
		en: 'Back',
		ru: 'Назад'
	},
	paymentYesBtn: {
		uk: 'Так',
		en: 'Yes',
		ru: 'Да'
	},
	paymentNoBtn: {
		uk: 'Ні',
		en: 'No',
		ru: 'Нет'
	},
	paymentResetBtn: {
		uk: 'Почати спочатку',
		en: 'Reset and start again',
		ru: 'Начать с начала'
	},
	paymentError: {
		uk: 'Виникла помилка, спробуйте трохи пізніше.',
		en: 'An error has occurred, try again later.',
		ru: 'Виникла ошибка, попробуйте немного позже.'
	},
	paymentStepperFirst: {
		uk: 'Виберіть платіжну систему',
		en: 'Select acquiring',
		ru: 'Выберите платежную систему'
	},
	paymentStepperSecond: {
		uk: 'Вкажіть суму платежу, рекомендовано',
		en: 'Enter amount, recommended',
		ru: 'Укажите сумму платежа, рекомендовано'
	},
	paymentStepperThird: {
		uk: 'Хочете скористатись акцією "10 + 2" ?',
		en: 'Do you want to use discount "10 + 2"?',
		ru: 'Хотите воспользоваться акцией "10 + 2"?'
	},
	paymentStepperFourth: {
		uk: 'Створити платіж',
		en: 'Make payment',
		ru: 'Создать платеж'
	},
	alreadyEnabledAutoPayment: {
		uk: 'У вас ввімкнено автоматичне списання з банківськоії карти по закінченню терміну підписки.',
		en: 'You have activated replenishment when finish subscription.',
		ru: 'У вас включено автоматическое списание с банковской картьі по окончанию термина подписки.'
	},
	removeAutoPayment: {
		uk: 'Вимкнути автоматичне списання з банківськоії карти?',
		en: 'Do you want to remove replenishment?',
		ru: 'Вьіключить автоматическое списание с банковской картьі?'
	},
	ignoreAutoPayment: {
		uk: 'Я все одно хочу заплатити',
		en: 'I wish to make payment',
		ru: 'Я все равно хочу заплатить'
	},
	deleteCamera: {
		uk: 'Видалити камеру?',
		en: 'Delete camera?',
		ru: 'Удалить кемеру?'
	},
	editCameraTitle: {
		uk: 'Назва камери',
		en: 'Title',
		ru: 'Название камеры'
	},
	editCameraDescription: {
		uk: 'Опис камери',
		en: 'Description',
		ru: 'Описания камеры'
	},
	editCameraSrc: {
		uk: 'Посилання',
		en: 'Source',
		ru: 'Ссылки'
	},
	newCamera: {
		uk: 'Додати нову камеру',
		en: 'Add new camera',
		ru: 'Дабавить новую камеру'
	},
	// newCameraText: {
	// 	uk: `Шановний абонент.<br />У цьому розділі ви можете самостійно ввести посилання на особисту ip-камеру.
	// 		Наприклад, камеру охорони периметра будинку або роботи. Всі введені посилання строго конфіденційні та доступні до перегляду тільки вам.
	// 		Посилання на "реальних" ip-адресах будуть доступні до перегляду поза домом / роботи, посилання на "сірих" ip-адресах (формат 10. *. *. * І 192.168. *. *) Будуть доступні тільки вдома (за роутером).
	// 		<br /><br />
	// 		Вбудований відеоплеєр вашого ТВ відтворює посилання з розширенням .mp4 та .m3u8
	// 		Якщо у вас поток з камери іншого формату, ми можемо провести його переформатування в потрібний формат. Послуга переформатування до 3 потоків безкоштовна.
	// 		Для замовлення напишіть нам на пошту webcam@hmara.tv`,
	// 	en: `Dear subscriber.<br />In this section you can independently enter the link to your personal ip-camera.
	// 		For example, the perimeter security camera at home or work. All entered links are strictly confidential and available for viewing only to you.
	// 		Links to the "real" ip-addresses will be available for viewing outside the home / work, links to the "gray" ip-addresses (format 10. *. *. * And 192.168. *. *) will be available only at home.
	// 		<br /><br />
	// 		The built-in video player of your TV plays links with the extension .mp4 and .m3u8
	// 		If you have a stream from a camera of a different format, we can recode it into the desired format. Recoding service up to 3 streams is free.
	// 		To order email us at webcam@hmara.tv`,
	// 	ru: `Уважаемый абонент.<br /> В этом разделе вы можете самостоятельно ввести ссылку на личную ip-камеру.
	// 		Например, камеру охраны периметра дома или работы. Все введенные ссылки строго конфиденциальны и доступны к просмотру только вам.
	// 		Ссылки на "реальных" ip-адресах будут доступны к просмотру вне дома/работы, ссылки на "серых" ip-адресах (формат 10.*.*.* и 192.168.*.*) будут доступны только дома (за роутером).
	// 		<br /><br />
	// 		Встроенный видеоплеер вашего ТВ воспроизводит ссылки с расширением .mp4 и .m3u8
	// 		Если у вас поток с камеры другого формата, мы можем провести ее переформатирование в нужный формат. Услуга  переформатирования  до 3 потоков бесплатна.
	// 		Для заказа напишите нам на почту webcam@hmara.tv`,
	// },
	doYouWantToChangeUser: {
		uk: 'Змінити користувача?',
		en: 'Do you want to change user?',
		ru: 'Сменить пользователя?'
	},
	cameraTooltipDelete: {
		uk: 'Видалити камеру',
		en: 'Delete this camera',
		ru: 'Удалить камеру'
	},
	cameraTooltipHide: {
		uk: 'Тимчасово приховати камеру',
		en: 'Hide camera',
		ru: 'Временно исключить камеру'
	},
	cameraTooltipTitle: {
		uk: 'Змінити назву',
		en: 'Change name of camera',
		ru: 'Изменить название'
	},
	cameraTooltipDescription: {
		uk: 'Змінити опис',
		en: 'Change description of camera',
		ru: 'Изменить описание'
	},
	cameraTooltipSrc: {
		uk: 'Змінити посилання',
		en: 'Change url of camera',
		ru: 'Изменить ссылку'
	},
	cameraTooltipScreenShot: {
		uk: 'Предперегляд камери',
		en: 'Camera preview',
		ru: 'Предпросмотр камеры'
	},
	referralDialogAgreeBtn: {
		uk: 'Продовжити',
		en: 'Continue',
		ru: 'Продолжить'
	},
	referralDialogDisagreeBtn: {
		uk: 'Ні, не цікаво',
		en: 'No, it isn\'t interested',
		ru: 'Нет, не интересно'
	},
	referralDialogHeader: {
		uk: 'Реферальне посилання',
		en: 'Referral link',
		ru: 'Реферальная ссылка'
	},
	promoRetrieveData: {
		uk: 'Будь ласка почекайте, йде отримання данних про промокод',
		en: 'Please wait, retrieving data from server ...',
		ru: 'Пожалуйста подождите, идет получение данных о промокоде'
	},
	promoDialogHeader: {
		uk: 'Промо код',
		en: 'Promo coupon',
		ru: 'Промо код'
	},
	promoPacket: {
		uk: 'пакет',
		en: 'packet',
		ru: 'пакет'
	},
	promoFreePeriod: {
		uk: 'Безоплатний період',
		en: 'Period for free',
		ru: 'Бесплатный период'
	},
	promoDiscount: {
		uk: 'Скидка',
		en: 'Discount',
		ru: 'Скидка'
	},
	promoNothing: {
		uk: 'Нема ніяких переваг для цього промо коду',
		en: 'This promo coupon hasn\'t got any data',
		ru: 'Нет никаких значений для этого промо кода'
	},
	promoSuccessful: {
		uk: 'Вітаємо. Ви отримали перелічені бонуси на свій рахунок',
		en: 'Congratulation. You have received bonuses that are described earlier',
		ru: 'Вы получили бонусы на свой счет'
	},
	promoFor: {
		uk: 'на',
		en: 'for',
		ru: 'на'
	},
	promoCheckBtn: {
		uk: 'натисніть для перевірки промокоду',
		en: 'Press to check discount code',
		ru: 'Нажмите для проверки промокода'
	},
	promoContinueBtn: {
		uk: 'Продовжити',
		en: 'Continue',
		ru: 'Продолжить'
	},
	promoUseAnotherCodeBtn: {
		uk: 'Використати другий промокод',
		en: 'Use another coupon',
		ru: 'Использовать другой промокод'
	},
	goToPortalInstallation: {
		uk: 'Перейти до установки порталу',
		en: 'Install portal',
		ru: 'Перейти к установке портала'
	},
	promoHOUR: {
		uk: 'годин',
		en: 'hours',
		ru: 'часов'
	},
	promoDAY: {
		uk: 'днів',
		en: 'days',
		ru: 'дней'
	},
	promoWEEK: {
		uk: 'тижнів',
		en: 'weeks',
		ru: 'недель'
	},
	promoMONTH: {
		uk: 'місяців',
		en: 'months',
		ru: 'месяцев'
	},
	promoYEAR: {
		uk: 'за рік',
		en: 'per a year',
		ru: 'за год'
	},
	ctypes: {
		tv: {
			uk: 'Телебачення',
			en: 'Live TV',
			ru: 'Телевидение'
		},
		video: {
			uk: 'Фільми',
			en: 'Movies',
			ru: 'Фильмы'
		},
		serial: {
			uk: 'Серіали',
			en: 'Series',
			ru: 'Сериалы'
		},
		cartoon: {
			uk: 'Мультфільми',
			en: 'Cartoon',
			ru: 'Мультфильмы'
		},
		radio: {
			uk: 'Радіо',
			en: 'Radio',
			ru: 'Радио'
		},
		camera: {
			uk: 'Веб камери',
			en: 'Web cameras',
			ru: 'Веб камеры'
		},
	},
	playListsDescription1: {
		uk: 'Тут ви можете отримати автоматично оновлювані списки відтворення (плейлист) для пристроїв, на яких не запускається наш портал. Впишіть вручну або перенесіть через флеш накопичувач дане посилання в пристрій. Підтримується функція одночасного перегляду на декількох пристроях.' ,
		en: 'Here you can download automatic updated playlists for devices which does not support our portal. Write down that url to device. Supports simultaneous viewing on multiple devices.',
		ru: 'Здесь вы можете скачать автоматически обновляемые списки воспроизведения (плейлист) для устройств, на которых не запускается наш портал. Впишите вручную или перенесите с помощью флешки в устройство. Поддерживается функция одновременного просмотра на нескольких устройствах.'
	},
	playListsDescription2: {
		uk: 'Якщо відтворення списку не відбувається перевірте кількість одночасно активних пристроїв.' ,
		en: 'If the list does not play, check the number of active devices.',
		ru: 'Если воспроизведения списка не происходит проверьте количество одновременно работающих устройств.'
	},
	cancelAllPlayLists: {
		uk: 'Закрити доступ до всіх плейлистів, що були скачані раніше',
		en: 'Disallow access to playlists that you have download before',
		ru: 'Закрыть доступ ко всем ранее скачанным плейлистам'
	},
	downloadPlayList: {
		uk: 'Скачати',
		en: 'Download',
		ru: 'Скачать'
	},
	makeShortUrl: {
		uk: 'Коротке посилання',
		en: 'Short  link',
		ru: 'Короткая ссылка'
	},
	activeDevicesBtn: {
		uk: 'Активні пристрої',
		en: 'Active devices',
		ru: 'Активные устройства'
	},
	activeDevicesListHeader: {
		uk: 'Список активних пристроїв:',
		en: 'List of active devices:',
		ru: 'Список активных устройств:'
	},
	activeDevicesDialogText1: {
		uk: 'Якщо у вас не відтворюються списки і з\'явилася підозра, що ваш список вкрадений, Ви можете подивитись на які пристрої встановлено список. Якщо бачите сторонні пристрої - є функція, яка дозволяє закрити підозрілу сесію або анулювати всі раніше завантажені списки.',
		en: 'If your lists are not reproduced and you suspect that your list has been stolen, you can check which devices are installed on the list. If you see unknown devices, there is a function that allows you to close a suspicious session or cancel any previously downloaded lists.',
		ru: 'Если у вас не воспроизводятся списки и появилась подозрение, что ваш список украден, Вы можете проверить, на какие устройства установлен список. Если видите сторонние устройства - есть функция, которая позволяет закрыть подозрительную сессию или аннулировать все ранее скачанные списки.'
	},
	activeDevicesDialogText2: {
		uk: 'Після завершення відображення контенту на пристрої, його активна сессія буде автоматично видалена після 2 хвилин бездіяльності.',
		en: 'If device don\'t work more than 2 minutes their active session will remove.',
		ru: 'После выключения устройства, его активная сессия будет автоматически удалена после 2 минут бездействия.'
	},
	activeDevicesDialogBtnRefresh: {
		uk: 'Оновити список',
		en: 'Refresh list',
		ru: 'Обновить список'
	},
	activeDevicesDialogBtnClose: {
		uk: 'Закрити',
		en: 'Close',
		ru: 'Закрыть'
	},
	activeDevicesConfirmDisallow: {
		uk: 'Ви впевнені, що хочети заборонити доступ по спискам відтворення, що скачали раніше?',
		en: 'Are you sure you want to disable access to playlists that were downloaded earlier?',
		ru: 'Вы уверены, что хочет закрыть доступ к спискам воспроизведения, что скачаны раньше?'
	},
}
